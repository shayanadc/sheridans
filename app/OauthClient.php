<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OauthClient extends Model
{
    public $incrementing = false;

    public function loginHistories(){
        return $this->hasMany('App\LoginHistory');
    }
}