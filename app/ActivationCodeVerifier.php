<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ActivationCodeVerifier extends Model{
    public function verify($identity, $code){
        $user = Auth::once(['username' => $identity, 'password' => $code]);
        if($user){
            return Auth::user()->id;
        }
        $user = User::withEmailOrMobile($identity)->first();
        $activationCode = $user->activationCode;
        if($activationCode && ($code == $activationCode->short_code || $code == $activationCode->long_code)){
            $activationCode->delete();
            return $user->id;
        }
            return false;
    }

}