<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use RuntimeException;
use Illuminate\Support\Collection;
use Authorizer;
class Order extends Model
{
    protected $casts = [
        'total_amount' => 'integer',
        'total_qty' => 'integer',
        'user_id' => 'integer',
    ];
    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function issuer(){
        return $this->belongsTo('App\User');
    }
    public function payments(){
        return $this->hasMany('App\Payment');
    }
    public function scopeUnpaid($query){
        $query->has("payments", '<', 1);
    }
    public function confirmedPayments(){
        return $this->hasMany('App\Payment')->confirmed();
    }

    public static function createFromDetails(Collection $details, User $user, User $issuer = null)
    {
        $issuer = $issuer?:$user;
        $order = new static();
        $order->user_id = $user->id;
        $order->issuer_id = $issuer->id;
        $order->total_amount = $details->reduce(function ($carry, $detail) {
            return $carry + $detail->subTotal;
        });
        $order->total_qty = $details->reduce(function ($carry, $detail) {
            return $carry + $detail->qty;
        });
        $order->save();
        $order->orderDetails()->saveMany($details);
        return $order;
    }

    public function getDateAttribute()
    {
        return $this->created_at;
    }

    public static function fromRows(User $customer, array $rows, User $issuer = null)
    {
        $details = OrderDetail::fromRows($rows);
        return static::createFromDetails($details, $customer, $issuer);
    }
    public function process(){
        foreach ($this->orderDetails as $detail){
            $detail->process();
        }
    }
}