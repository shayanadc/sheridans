<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const SUPER_ADMIN_ROLE = 1;
    const ADMIN_ROLE = 2;
    const SUPERVISOR_ROLE = 3;
    const SALES_ROLE = 4;
    const ACCOUNTANT_ROLE = 5;
    const SUPPORT_ROLE = 6;
    const CUSTOMER_ROLE = 7;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function registerPayments()
    {
        return $this->morphMany('App\Payment', 'registrant');
    }
    public function payments()
    {
        return $this->hasManyThrough('App\Payment', 'App\Order', 'user_id', 'order_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function issuedOrders()
    {
        return $this->hasMany('App\Order', 'issuer_id');
    }

    public function activationCode()
    {
        return $this->hasOne('App\ActivationCode')->notExpired();
    }
    public function phones()
    {
        return $this->belongsToMany('App\Phone')->withPivot('default');
    }

    public function landLines()
    {
        return $this->belongsToMany('App\Phone')->landLines()->withPivot('default');
    }

    public function mobiles()
    {
        return $this->belongsToMany('App\Phone')->mobiles()->withPivot('default');
    }

    public function getDefaultMobileAttribute()
    {
        return $this->mobiles()->wherePivot('default', true)->first();
    }

    public function getDefaultLandLineAttribute()
    {
        return $this->landLines()->wherePivot('default', true)->first();
    }

    public function setDefaultMobile(Phone $mobile)
    {
        if ($this->defaultMobile)
            $this->mobiles()->updateExistingPivot($this->defaultMobile->id, ['default' => false]);
        if ($this->mobiles->contains($mobile))
            $this->mobiles()->updateExistingPivot($mobile->id, ['default' => true]);
        else
            $this->mobiles()->save($mobile, ['default' => true]);
    }
    public function setDefaultLandLine(Phone $landLine)
    {
        if ($this->defaultLandLine)
            $this->landLines()->updateExistingPivot($this->defaultLandLine->id, ['default' => false]);
//        $this->landLines()->updateExistingPivot($mobile->id, ['default' => true]);
        if ($this->landLines->contains($landLine))
            $this->landLines()->updateExistingPivot($landLine->id, ['default' => true]);
        else
            $this->landLines()->save($landLine, ['default' => true]);
    }

    public function hasActivationCode()
    {
        return $this->activationCode !== null;
    }

    public static function generateUsername()
    {
        do {
            return $username = strval(random_int(100000, 999999));
        } while (static::where('username', $username)->count() > 0);
    }

    public static function generatePassword()
    {
        return $password = strval(random_int(100000, 999999));
    }

    public static function createWithEmail($email)
    {
        return static::Create([
            'email' => $email,
            'username' => static::generateUsername(),
            'password' => bcrypt(static::generatePassword())
        ]);
    }

    public static function createWithMobile($mobile)
    {
        return static::Create([
            'mobile' => $mobile,
            'username' => static::generateUsername(),
            'password' => bcrypt(static::generatePassword())
        ]);
    }

    public static function findOrCreateWithEmail($email)
    {
        $user = static::where('email', $email)->first();
        if (is_null($user)) {
            return static::createWithEmail($email);
        }
        return $user;
    }

    public static function findOrCreateWithMobile($mobile)
    {
        $user = static::where('mobile', $mobile)->first();
        if (is_null($user)) {
            return static::createWithMobile($mobile);
        }
        return $user;
    }

    /**
     * Scopes
     */
    public function scopeWithEmailOrMobile($query, $identity)
    {
        return $query->where('mobile', $identity)->orWhere('email', $identity);
    }
}
