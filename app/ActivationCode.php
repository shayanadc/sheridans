<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ActivationCode extends Model{
    protected $fillable = ['long_code','short_code'];
    const EXPIRE_INTERVAL = 20;

    /**
     * @return string
     */
    protected static function generateShortCode()
    {
        return strval(random_int(100000, 999999));
    }

    public static function generateLongCode(){
        do {
            return $long_code = str_random(40);
        } while (static::where('long_code', $long_code)->count() > 0);
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function scopeExpired($query, $duration = self::EXPIRE_INTERVAL){
        return $query->where('created_at', '<', Carbon::now()->subMinutes($duration));
    }
    public static function scopeNotExpired($query, $duration = self::EXPIRE_INTERVAL){
        return $query->where('created_at', '>=', Carbon::now()->subMinutes($duration));
    }
    public static function deleteExpired($duration = self::EXPIRE_INTERVAL){
        $ids = static::expired($duration)->get()->pluck('id');
        if (! $ids->isEmpty())
            return self::destroy($ids);
    }
    public static function createFor(User $user){
        self::deleteExpired();
        $ac = new static();
        $ac->generateCodes();
        $user->activationCode()->save($ac);
        return $ac;
    }

    public function generateCodes()
    {
        $this->long_code = self::generateLongCode();
        $this->short_code = self::generateShortCode();
    }

}