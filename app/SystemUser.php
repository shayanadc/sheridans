<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemUser extends Model
{
    public function payments()
    {
        return $this->morphMany('App\Payment', 'registrant');
    }
    public function orderDetails()
    {
        return $this->morphMany('App\OrderDetail', 'assignee');
    }
}
