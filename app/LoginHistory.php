<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
    protected $fillable = ['user_id', 'oauth_client_id'];

    public static function register(User $user, OauthClient $oauthClient){
        $lastLogin = static::firstOrCreate(['user_id' => $user->id, 'oauth_client_id' => $oauthClient->id]);
        $lastLogin->touch();
        return $lastLogin;
    }
    public function oauthClient(){
        return $this->belongsTo('App\OauthClient');
    }
}
