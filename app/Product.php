<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function entity()
    {
        return $this->morphTo();
    }
    public function getTitleAttribute()
    {
        return $this->entity->title;
    }
    public function getDescriptionAttribute()
    {
        return $this->entity->description;
    }
    public function getPriceAttribute()
    {
        return $this->entity->price;
    }
    public function getVisibleAttribute()
    {
        return $this->entity->attributes['visible'] == "1";
    }
    public function orderDetails(){
        return $this->hasMany('App\OrderDetail');
    }
    public function productType(){
        return $this->belongsTo('App\ProductType');
    }
}
