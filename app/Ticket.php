<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['user_id', 'err_code', 'closed'];

    protected $casts = [
        'user_id' => 'integer',
        'closed' => 'boolean',
        'err_code' => 'integer',
    ];

    public static function add(User $user, TicketCode $code)
    {
        return static::create([
            'user_id' => $user->id,
            'err_code' => $code->id,
            'closed' => false
        ]);
    }

    public function close()
    {
        return $this->update(['closed' => true]);
    }

    public function scopeOpened($query)
    {
        $query->where('closed', false);
    }

    public function scopeClosed($query)
    {
        $query->where('closed', true);
    }
}
