<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const MANUAL_METHOD = 1;
    const ONLINE_METHOD = 2;
    protected $fillable = ['amount', 'gw', 'method', 'registrant_id', 'registrant_type', 'confirmed', 'tracking_code'];
    protected $casts = [
    'confirmed' => 'boolean'
    ];

    public function systemUser()
    {
        return $this->morphTo();
    }
    public function registrant()
    {
        return $this->morphTo();
    }
    public function order(){
        return $this->belongsTo('App\Order');
    }
    public function scopeConfirmed($query){
        $query->where('confirmed', true);
    }
    public function gateway(){
        return $this->belongsTo('App\PaymentGateway', 'id');
    }
}
