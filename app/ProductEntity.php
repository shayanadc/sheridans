<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class ProductEntity extends Model
{
    public function provider(){
      return  $this->belongsTo('App\Provider');
    }
    public function orderDetails()
    {
        return $this->morphMany('App\OrderDetail', 'product');
    }
    public function products()
    {
        return $this->morphMany('App\Product', 'entity');
    }
    public function scopeVisible($query){
        return $query->where('visible', true);
    }
    public function queueable(){
        return true;
    }
    public function pgs(){
        $pc = ProductClass::where('name', static::class)->first();
        return $pc->getPaymentGateWays();
    }
}