<?php

namespace App;

class PartaakProduct extends ProductEntity
{
    const TRAFFIC_CATEGORY = 1;
    const SERVICE_CATEGORY = 2;
    protected $fillable = ['title', 'description', 'price', 'visible', 'provider_id','rate', 'vol', 'period', 'rate_scale', 'scale', 'ussd_order'];

    protected $casts = [
        'price' => 'integer',
        'visible' => 'boolean',
        'id' => 'integer',
        'provider_id' => 'integer',
        'vol' => 'integer',
        'rate' => 'integer',
        'period' => 'integer',
    ];
    public function scopeTraffic($query){
        return $query->where('category', static::TRAFFIC_CATEGORY);
    }
    public function scopeService($query){
        return $query->where('category', static::SERVICE_CATEGORY);
    }
    public function scopeGreaterThan2Mbps($query){
        return $query->where('rate', '>', 2048);
    }
    public function scopeLessThan2Mbps($query){
        return $query->where('rate', '<=', 2048);
    }
    public function process(OrderDetail $detail,array $param = null){
        $partaakCRM  = new PartaakCRM();
        $partaakCRM->process($detail,$param);
    }
}
