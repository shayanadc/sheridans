<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ['number', 'mobile'];
    protected $casts = [
        'id' => 'integer',
        'mobile' => 'boolean'
    ];
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('default');
    }
    public function defaultUsers(){
        return $this->belongsToMany('App\User')->withPivot('default')->wherePivot('default', true);
    }
    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function scopeMobiles($query){
        $query->where('mobile', true);
    }
    public function scopeLandLines($query){
        $query->where('mobile', false);
    }
    public static function findServiceProvider($phone){
        $opt = 'noop';
        if (preg_match('#^((\+|00)98)|0?9(30|33|35|36|37|38|39|03|01|90)\d{7}$#', $phone)) {
            $opt = 'mtn';
        }
        if (preg_match('#^((\+|00)98)|0?91[0-9]\d{7}$#', $phone)) {
            $opt = 'mci';
        }
        if (preg_match('#^((\+|00)98)|0?92(0|1)\d{7}$#', $phone)) {
            $opt = 'rgt';
        }
        if (preg_match('#^((\+|00)98)|0?932\d{7}$#', $phone)) {
            $opt = 'talia';
        }

        return $opt;
    }
}
