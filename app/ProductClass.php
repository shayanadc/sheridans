<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductClass extends Model
{
    public function paymentGateways(){
        return $this->belongsToMany('App\PaymentGateway');
    }
    public function getPaymentGateways(){
        if($this->paymentGateWays->isEmpty()){
            return PaymentGateway::getFallbackGateways();
        }
        return $this->paymentGateWays;
    }
}
