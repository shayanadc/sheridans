<?php
/**
 * Created by PhpStorm.
 * User: shayan
 * Date: 4/6/16
 * Time: 3:02 PM
 */
namespace App;

class PayamResan {
    protected $client;
    protected $parameters;

    public function __construct() {
        $this->client = new \SoapClient('http://sms-webservice.ir/v1/v1.asmx?WSDL', array('cache_wsdl' => 'WSDL_CACHE_MEMORY'));

        $this->parameters['Username'] = "09111704575";
        $this->parameters['PassWord'] = "wed875milt724";
        $this->parameters['SenderNumber'] = "50002060555050";
    }

    public function sendMessage($mobile, $message) {
        $this->parameters['RecipientNumbers'] = array($mobile);
        $this->parameters['MessageBodie'] = $message;
        $this->parameters['Type'] = 1;
        $this->parameters['AllowedDelay'] = 0;

        $result = $this->client->SendMessage($this->parameters)->SendMessageResult->long;

        if($result <= 0){
            throw new \Exception('PayamResan Fail To Sending SMS For Error: ' , compact('result'));
        }
        return true;
    }
    public function getMessagesStatus($messageId) {
        $this->parameters['messagesId'] = $messageId;
        $res = $this->client->GetMessagesStatus($this->parameters);
        return $res;
    }
    public function getCredit(){
        $credit = $this->client->GetCredit($this->parameters);
        if($credit > 0){
            return $credit;
        }else{
            throw new \Exception('Payam Resan Has Not Credit!!');
        }
    }
}