<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = ['name', 'area_code', 'visible'];
    protected $casts = [
        'visible' => 'boolean'
    ];
    public function cities()
    {
        return $this->hasMany('App\City');
    }
    public function scopeVisible($query){
        return $query->where('visible', true);
    }
    public function scopeInVisible($query){
        return $query->where('visible', false);
    }

}
