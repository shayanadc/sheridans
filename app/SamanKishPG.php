<?php
/**
 * Created by PhpStorm.
 * User: faramarz
 * Date: 12/11/15
 * Time: 12:23
 */

namespace App;

use App\Exceptions\PGCallBackException;
use App\Exceptions\PGVerificationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SoapClient;
use Log;

class SamanKishPG
{
    protected static $gatewayUrl = 'https://sep.shaparak.ir/Payment.aspx';
    private static $gatewayName = 'سامان کیش';

    protected $mid;
    protected $pass;
    protected $callback;

    public function __construct(PaymentGateway $paymentGateway)
    {
        $this->mid = $paymentGateway->mid;
        $this->pass = $paymentGateway->password;
        $this->callback = 'payment_callback/' . $paymentGateway->id;
    }

    public function getCallbackUrl()
    {
        return url($this->callback);
    }

    public static function getGatewayUrl()
    {
        return static::$gatewayUrl;
    }

    public function getMid()
    {
        return $this->mid;
    }

    public static function getGatewayName()
    {
        return static::$gatewayName;
    }

    public function getRequestParamsFor(Order $order)
    {
        return [
            'url' => static::$gatewayUrl,
            'data' => [
                'Amount' => $order->total_amount,
                'MID' => $this->mid,
                'ResNum' => $order->id,
                'RedirectURL' => url($this->callback),
            ],
        ];
    }
    public function callbackHandler(Request $request)
    {
        $payment = $this->getPayment($request);

        Log::info('Payment: ', compact('payment'));
        if ($payment->state == "OK") {
            $payment = static::verify($payment);
        }
        else {
            throw new PGCallBackException();
        }

        return $payment;
    }

    /**
     * @param $payment
     * @throws PGVerificationException
     */
    public function verify(\app\OnlinePayment $payment)
    {
        $soapClient = new SoapClient('https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL');

        $result = $soapClient->VerifyTransaction($payment->refnum, $this->mid);

        if ($result <= 0) {
            throw new PGVerificationException();
        } else {
            $payment->amount = $result;
        }

        return $payment;
    }

    public function getPayment(Request $request)
    {
        $payment = new OnlinePayment();

        $validator = Validator::make($request->all(), [
            'State'  => 'required',
            'RefNum' => 'required|unique:invoices,tracking_code',
            'ResNum' => 'required',
        ]);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(trans('messages.pgCallBackException'));
        }

        $payment->state = $request->input('State');
        $payment->refnum = $request->input('RefNum');
        $payment->resnum = $request->input('ResNum');
        $payment->traceno = $request->input('TRACENO');
        return $payment;
    }
}