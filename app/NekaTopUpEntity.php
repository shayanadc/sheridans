<?php

namespace App;

class NekaTopUpEntity extends ProductEntity
{
    protected $fillable = ['title', 'description', 'price', 'visible', 'provider_id','operator', 'special'];

    protected $casts = [
        'price' => 'integer',
        'visible' => 'boolean',
        'id' => 'integer',
        'provider_id' => 'integer',
        'special' => 'boolean',
    ];
    public function scopeSpecial($query){
        return $query->where('special', true);
    }
    public function scopeNormal($query){
        return $query->where('special', false);
    }
    public function scopeServiceProvider($query, $serviceProvider){
        return $query->where('operator', $serviceProvider);
    }
    public static function getSalesProducts($mobile){
        $sp = Phone::findServiceProvider($mobile);
        $topUps = self::serviceProvider($sp)->visible()->get();
        return $topUps;
    }
}
