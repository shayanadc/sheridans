<?php

namespace App\Http\Controllers;

use App\PartaakCRM;
use App\PartaakProduct;
use App\Phone;
use App\NekaTopUpEntity;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    public function getAllProductsWith($pType, $phone = null){
        if($pType == 'topup') {
            try{
            $sp = Phone::findServiceProvider($phone);
            $topUpSpecial = NekaTopUpEntity::serviceProvider($sp)->special()->get();
            $topUpNormal = NekaTopUpEntity::serviceProvider($sp)->normal()->get();
            $products = [
                'normal' => $topUpNormal,
                'special' => $topUpSpecial
            ];
            }catch (\Exception $e){
                return [
                    'result' => 'FAIL',
                    'message' => '',
                ];
            }
        }
        if($pType == 'adsl') {
            if (strpos($phone, "11") === 0) {
                try{
                    $traffics = PartaakCRM::allowedTraffic($phone);
                    $services = PartaakCRM::allowedService($phone);
                    $products = [
                        'traffics' => $traffics,
                        'services' => $services,
                    ];
                }catch (\Exception $e){
                    return [
                        'result' => 'FAIL',
                        'message' => '',
                    ];
                }
            }
        }
            return [
                'result' => 'OK',
                'message' => '',
                'products' => $products,
                'provider' => 'پارتاک استان م- ازندران'
            ];

    }
    public function getStatement($pType, $phone)
    {
        if ($pType == 'adsl') {
            if (strpos($phone, "11") === 0) {
                try {
                    $remainDays = PartaakCRM::getDaysRemaining($phone);
                    $remainVol = PartaakCRM::getVolRemaining($phone);
                    return [
                        'result' => 'OK',
                        'message' => '',
                        'remainDays' => $remainDays,
                        'remainVol' => $remainVol
                    ];
                } catch (\Exception $e) {
                    return [
                        'result' => 'FAIL',
                        'message' => '',
                    ];
                }
            }
        }
        return [
            'result' => 'FAIL',
            'message' => '',
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
