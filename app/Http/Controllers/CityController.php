<?php

namespace App\Http\Controllers;

use App\City;
use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::with('province')->get();
        return [
            'result' => 'ok',
            'message' => 'all cities with provinces',
            'cities' => $cities->toArray()
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $province = Province::findOrFail($request->input('province_id'));
        $city = City::create([
            'name' => $request->input('name'),
            'province_id' => $province->id
        ]);
        $city = City::where('id', $city->id)->with('province')->first();
        return [
            'result' => 'ok',
            'message' => 'city ' . $city->id . ' has been saved',
            'city' => $city->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->update(['province_id' => $request->input('province_id'), 'name' => $request->input('name')]);
        return [
            'result' => 'ok',
            'message' => 'city ' . $city->id . ' has been updated',
            'city' => $city->toArray()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
