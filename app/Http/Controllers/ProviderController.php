<?php

namespace App\Http\Controllers;

use App\City;
use App\Provider;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $providers = Provider::with('cities')->get();
        return [
            'result' => 'ok',
            'message' => 'all providers with cities',
            'providers' => $providers->toArray()
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider = Provider::create([
            'title' => $request->input('title'),
            'name' => $request->input('name')
        ]);
        foreach($request->input('cities_id') as $city){
            $provider->cities()->attach($city);
        }
        $provider = Provider::find($provider->id);
        return [
            'result' => 'ok',
            'message' => 'provider has been updated',
            'provider' => $provider->toArray(),
            'cities' => $provider->cities->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::with('cities')->find($id);
        return [
            'result' => 'ok',
            'message' => 'cities of provider',
            'provider' => $provider->toArray(),
            'cities' => $provider->cities->toArray()
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getProviders($cityId, $type = null){
        $city = City::findOrFail($cityId);
        $providers = $city->providers;
        if(!is_null($type)){
            $providers = $city->providers()->where('title', $type)->get();
        }
        if($providers){
            return [
                'result' => 'OK',
                'message' => '',
                'providers' => $providers->toArray(),
            ];
        }else{
            return [
                'result' => 'FAIL',
                'message' => '',
            ];
        }
    }
}
