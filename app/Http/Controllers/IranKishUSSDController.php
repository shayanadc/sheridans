<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use App\PartaakCRM;
use App\PartaakProduct;
use App\Payment;
use App\Phone;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IranKishUSSDController extends Controller
{
    public function getServicesFor($phone, $lang, $mobile)
    {
        \App::setLocale($lang);
        try {
            $partaakP = PartaakCRM::allowedTraffic($phone);
            $currentService = PartaakCRM::currentService($phone);
            if(!is_null($currentService)){
                $currentServiceTitle = $currentService->rate . 'مگ' . $currentService->vol . 'گیگ'. "تمدید سرویس " . " به مدت " . $currentService->period . "ماه" . "\n 1-تایید";
            }else{
                $currentServiceTitle= "تمدید سرویس جاری امکانپذیر نیست.";
            }
        } catch (\Exception $e) {
            $partaakP = null;
        }
        if ($partaakP && $partaakP->count() > 0) {
            $products = PartaakProduct::where('ussd_order', '>', 0)->orderBy('ussd_order')->get();
            $traffics = [];
            foreach ($products as $index => $product) {
                $traffics[] = $index + 1 . '-' . $product->vol . " " . trans('ussd.gig');
            }
            return [
                'desc' => trans('ussd.menu'),
                'stat' => true,
                '1' => [
                    'desc' => implode("\n", $traffics),
                    'stat' => true
                ],
                '2' => [
                    'desc' => $currentServiceTitle,
                    'stat' => true
                ],
                '3' => [
                    'desc' => "لطفا شماره صورتحساب خود را وارد کنید",
                    'stat' => true
                ]
            ];
        } else {
            return [
                'desc' => "ارائه خدمات به شماره مورد نظر امکانپذیر نیست.",
                'stat' => false
            ];
        }
    }

    public function getServiceCostFor($shortCode, $lang, $mobile)
    {
        \App::setLocale($lang);
        list($phone, $type, $requestId) = explode('*', $shortCode);
        $user = User::findOrCreateWithMobile($mobile);
        $phone = Phone::firstOrCreate(['number' => $phone]);
        if(! $user->phones->contains($phone)){
            $user->phones()->attach($phone->id);
        }
        if ($type == '1') {
            $product = PartaakProduct::where('ussd_order', $requestId)->first();
            $description = $product->title . " برای شماره سرویس" . $phone->number . " به مبلغ " . $product->price . " ریال " . "\n1-تایید";
            $params = [['product' => $product, 'qty' => 1, 'phone' => $phone]];
            $order = Order::fromRows($user, $params);
            if ($product) {
                return [
                    'desc' => $description,
                    'stat' => true,
                    'amount' => $order->total_amount,
                    'invoiceNumber' => $order->id
                ];
            } else {
                return [
                    'desc' => "سرویس درخواست شده معتبر نمیباشد.",
                    'stat' => false
                ];
            }
        }
        if($type == '2'){
            $product = PartaakCRM::currentService($phone->number);
            $description = $product->title . " برای شماره سرویس" . $phone->number . " به مبلغ " . $product->price . " ریال " . "\n1-تایید";
            $params = [['product' => $product, 'qty' => 1, 'phone' => $phone]];
            $order = Order::fromRows($user, $params);
            if ($order) {
                return [
                    'desc' => $description,
                    'stat' => true,
                    'amount' => $order->total_amount,
                    'invoiceNumber' => $order->id
                ];
            } else {
                return [
                    'desc' => "سرویس درخواست شده معتبر نمیباشد.",
                    'stat' => false
                ];
            }
        }
        //order payment
        if ($type == '3') {
            try{
                //scope condition to pay order
                $order = Order::findOrFail($requestId);
                $od = $order->orderDetails()->first();
                $description = $od->product->title . " برای شماره سرویس" . $od->phone->number . " به مبلغ " . $od->product->price . " ریال " . "\n1-تایید";
                return [
                    'desc' => $description,
                    'stat' => true,
                    'amount' => $order->total_amount,
                    'invoiceNumber' => $order->id
                ];
            } catch (\Exception $e) {
                return [
                    'desc' => "سرویس درخواست شده معتبر نمیباشد.",
                    'stat' => false
                ];
            }
        }
    else {
            return [
//                'desc' => "سرویس درخواست شده معتبر نمیباشد.",
                'stat' => false
            ];
        }

    }

    /**
     * @param Request $request
     * @return array
     */
    public function callbackHandler(Request $request)
    {
        //check request has valid name
        \App::setLocale($request->input('lang'));
        if ($request->get('resultCode') == '100') {
            $order = Order::findOrFail($request->input('invoiceNumber'));
            // register payment gateway
            $order->payments()->save(new Payment([
                'gw' => 1,
                'tracking_code' => $request->input('referenceCode'),
                'confirmed' => true,
                'amount' => $order->total_amount,
                'method' => 'USSD'
            ]));
            // dispatch order into queue
            $order->process();
            return [
                    'stat' => true
                ];
            } else {
                return [
                    'stat' => false
                ];
            }
        }
}
