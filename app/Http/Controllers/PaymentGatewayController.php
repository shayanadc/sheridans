<?php

namespace App\Http\Controllers;

use App\Order;
use App\PaymentGateway;
use Illuminate\Http\Request;
use ReflectionClass;

use App\Http\Requests;

class PaymentGatewayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gws = PaymentGateway::all();
        return [
            'result' => 'ok',
            'message' => 'all payment gateways',
            'gateways' => $gws->toArray()
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gw = PaymentGateway::create($request->all());
        $gw = PaymentGateway::findOrFail($gw->id);
        return [
            'result' => 'ok',
            'message' => 'new gateway has been saved',
            'gateway' => $gw->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gw = PaymentGateway::findOrFail($id);
        return [
            'result' => 'ok',
            'message' => 'returned'. $gw->id,
            'gateway' => $gw->toArray()
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gw = PaymentGateway::findOrFail($id);
        $gw->update($request->all());
        $gateway = PaymentGateway::findOrFail($id);
        return[
            'result' => 'ok',
            'message' => 'gateway ' . $gateway->id . ' has been updated',
            'gateway' => $gateway->toArray()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getGwParamsForOrder($gwId, $orderId){
        //find order
        $order = Order::find($orderId);
        //find gateway
        $gw = PaymentGateway::findOrFail($gwId);
        //exist relation between order and gw
        $gws = $order->orderDetails->first()->product->pgs();
        if($gws->contains($gw)){
            $gwClass = $gw->name;
            $pspClass = new \ReflectionClass($gwClass);
            $psp = $pspClass->newInstanceArgs([$gw]);
            return [
                'result' => 'OK',
                'message' => '',
                'params' => $psp->getRequestParamsFor($order)
            ];
        }
        return [
            'result' => 'FAIL',
            'message' => 'invalid gw for this order'
        ];
    }
}
