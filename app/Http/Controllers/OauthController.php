<?php

namespace App\Http\Controllers;

use Authorizer;
use Response;
use Illuminate\Http\Request;

use App\Http\Requests;

class OauthController extends Controller
{
    public function access(Request $request) {
        return Response::json(Authorizer::issueAccessToken());
    }

    public function revoke()
    {
        try {
            Authorizer::getChecker()->getAccessToken()->expire();
            return [
                'result' => 'OK',
                'message' => 'Tokens revoked'
            ];
        } catch (\Exception $e) {
            return [
                'result' => 'FAIL',
                'message' => $e->getMessage()
            ];
        }
    }
}
