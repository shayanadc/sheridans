<?php

namespace App\Http\Controllers;

use App\Order;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Authorizer;
use App\Http\Requests;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user  = User::findOrFail(Authorizer::getResourceOwnerId());
        $order = Order::findOrFail($request->input('order_id'));
        $payment =  Payment::create([
            'order_id' => $order->id,
            'amount' => $order->total_amount,
            'gw' => $request->input('gw'),
            'method' => \App\Payment::MANUAL_METHOD,
            'pan' => $request->input('pan'),
            'tracking_code' => $request->input('tracking_code'),
            'receipt_number' => $request->input('receipt_number'),
            'registrant_id' => $user->id,
            'registrant_type' => get_class($user)
        ]);
        $payment = Payment::findOrFail($payment->id);
        return [
            'result' => 'OK',
            'message' => 'payment' . $payment->id .'returned',
            'payment' => $payment->toArray(),
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::find($id);
        return [
            'result' => 'OK',
            'message' => 'payment' . $payment->id .'returned',
            'payment' => $payment->toArray(),
            'order' => $payment->order->toArray()
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payment = Payment::findOrFail($id);
        $payment->update($request->all());
        return [
            'result' => 'OK',
            'message' => 'payment' . $payment->id .'returned',
            'payment' => $payment->toArray(),
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function userPayments(){
        $user = Authorizer::getResourceOwnerId();
        $user = User::findOrFail($user);
        if ($user) {
            return [
                'result' => 'OK',
                'message' => '',
                'payments' => $user->payments
            ];
        }
    }
}
