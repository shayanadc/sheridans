<?php

namespace App\Http\Controllers;

use App\TicketCode;
use Illuminate\Http\Request;

use App\Http\Requests;

class TicketCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ticketCodes =  TicketCode::all();
        return [
                'result' => 'ok',
                'message' => 'all ticket codes',
                'ticketCodes' => $ticketCodes
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticketCode = TicketCode::findOrFail($id);
        $ticketCode->update($request->all());
        return [
            'result' => 'ok',
            'message' => 'ticket code' . $ticketCode->id. 'has been updated',
            'ticketCode' => $ticketCode->toArray()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * return specific visible ticketCodes
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function visible(){
            $ticketCodes =  TicketCode::visible()->get();
            return [
                'result' => 'ok',
                'message' => 'all ticket codes',
                'ticketCodes' => $ticketCodes
            ];

    }
}
