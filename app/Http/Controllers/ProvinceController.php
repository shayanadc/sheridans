<?php

namespace App\Http\Controllers;

use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::all();
        return [
            'result' => 'ok',
            'message' => 'all provinces',
            'provinces' => $provinces->toArray()
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $province = Province::create([
            'name' => $request->input('name'),
            'area_code' => $request->input('area_code')
        ]);
        $province = Province::findOrFail($province->id);
        return[
            'result' => 'ok',
            'message' => 'ticket ' . $province->id . ' has been saved',
            'province' => $province->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $province = Province::findOrFail($id);
        $province->update(['visible' => $request->input('visible'), 'area_code' => $request->input('area_code'), 'name' => $request->input('name')]);
        return [
            'result' => 'ok',
            'message' => 'province ' . $province->id . ' has been updated',
            'province' => $province->toArray()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function visible()
    {
        $provinces = Province::visible()->with('cities')->get();
        return [
            'result' => 'ok',
            'message' => 'all visible provinces',
            'provinces' => $provinces->toArray()
        ];
    }
}
