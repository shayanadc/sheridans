<?php

namespace App\Http\Controllers;

use App\NekaTopUpEntity;
use App\Order;
use App\Phone;
use App\Product;
use App\ProductClass;
use App\User;
use Illuminate\Http\Request;
use Authorizer;
use App\Http\Requests;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rows = $request->input('rows');
        $user = User::findOrFail(Authorizer::getResourceOwnerId());
        if($user->role == User::SUPPORT_ROLE || $user->role == User::SUPERVISOR_ROLE){
            $issuer = $user;
            $customer = User::findOrFail($request->input('customer_id'));
        }else{
            $issuer = null;
            $customer = User::findOrFail(Authorizer::getResourceOwnerId());
        }
        foreach($rows as $row){
            $phone = null;
            if(isset($row['params']['land_line']) || isset($row['params']['mobile'])){
                $mobile = isset($row['params']['mobile'])? true : false;
                $number = isset($row['params']['mobile'])? $row['params']['mobile'] : $row['params']['land_line'];
                $phone = Phone::create([
                    'number' => $number,
                    'mobile' => $mobile
                ]);
                $customer->phones()->attach([
                    $phone->id
                ]);
            }
            $qty = isset($row['qty'])? $row['qty'] : null;
            $product = Product::findOrFail($row['id']);
            $items[] = ['product' => $product, 'qty' => $qty, 'params' => ['phone' => $phone]];
        }
        $order = Order::fromRows($customer, $items, $issuer);
        return [
            'result' => 'OK',
            'message' => '',
            'order' => [
                'id' => $order->id,
                'total_amount' => $order->total_amount,
                'user_id' => $order->user_id,
                'issuer_id' => $order->issuer_id,
            ],
            'gateways' => $order->orderDetails->first()->product->entity->pgs()->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        return [
            'result' => 'OK',
            'message' => '',
            'order' => $order->toArray(),
            'gateways' => $order->orderDetails->first()->product->entity->pgs()->toArray()
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *g
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unpaid($type = null, $number = null){
        if($type == 'phone'){
            $phone = Phone::where('number', $number)->first();
            if($phone){
                $ods = $phone->orderDetails;
                foreach($ods as $od){
                    $coll[] = $od->order_id;
                }
                $orders = Order::whereIn('id', $coll)->unpaid()->get();
                return [
                    'result' => 'OK',
                    'message' => '',
                    'orders' => $orders->toArray()
                ];
            }
            return [
                'result' => 'OK',
                'message' => '',
                'orders' => null
            ];
        }elseif(is_null($type) && is_null($number)){
            $user = Authorizer::getResourceOwnerId();
            if ($user) {
                 $orders = Order::where('user_id', $user)->unpaid()->get();
                return [
                    'result' => 'OK',
                    'message' => '',
                    'orders' => $orders->toArray()
                ];
            }
        }
        return [
            'result' => 'FAIL',
            'message' => '',
        ];
    }
}
