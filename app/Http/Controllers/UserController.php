<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Validator;
use Authorizer;

class UserController extends Controller
{
    public function owner()
    {
        $user = Authorizer::getResourceOwnerId();
        if ($user) {
            return User::find($user);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('mobiles', 'landLines')->get();
        return [
            'result' => 'OK',
            'message' => '',
            'users' => $users->toArray()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usename = $request->input('username');
        $validator = Validator::make([
            'username' => $usename,
        ], [
            'username' => 'unique:users'
        ], [
            'username.unique' => 'نام کاربری تکراری است.',
        ]);
        if ($validator->fails()) {
            $info = array(
                'result' => 'FAIL',
                'message' => $validator->errors()->first(),
            );
            return $info;
        }
        $user = User::create([
            'username' => $usename,
            'password' => bcrypt($request->input('password')),
            'email' => $request->input('email'),
            'mobile' => $request->input('mobile')
        ]);
        $user = User::find($user->id);
        return [
            'result' => 'OK',
            'message' => '',
            'user' => $user->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return [
            'result' => 'OK',
            'message' => '',
            'user' => $user->toArray(),
            'landLines' => $user->landLines->toArray(),
            'mobiles' => $user->mobiles->toArray(),
            'defaultLandLine' => $user->defaultLandLine,
            'defaultMobile' => $user->defaultMobile
        ];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return [
            'result' => 'OK',
            'message' => '',
            'user' => $user->toArray(),
            'landLines' => $user->landLines->toArray(),
            'mobiles' => $user->mobiles->toArray(),
            'defaultLandLine' => $user->defaultLandLine,
            'defaultMobile' => $user->defaultMobile
        ];
    }
    public function authUser()
    {
        $userId = Authorizer::getResourceOwnerId();
        return $this->show($userId);
    }
    public function editProfile(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        return  $this->update($request, $userId);
    }
}