<?php

namespace App\Http\Controllers;

use App\ActivationCode;
use App\Jobs\SendSms;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\User;
use Mail;

class ActivationAuthController extends Controller
{
    public function auth(Request $request)
    {
        $email = $request->input('email');
        $mobile = $request->input('mobile');
        $validator = Validator::make([
            'email' => $email,
            'mobile' => $mobile,
        ], [
            'email' => 'email',
            'mobile' => 'digits:11',
        ], [
            'email.email' => 'ایمیل وارد شده معتبر نیست.',
            'mobile.digits' => 'شماره موبایل وارد شده معتبر نیست.',
        ]);
        if ($validator->fails()) {
            $info = array(
                'result' => 'FAIL',
                'message' => $validator->errors()->first(),
            );
            return $info;
        }
//        $activationCode = '';
        if ($request->has('email')) {
            $user = User::findOrCreateWithEmail($email);
            if(!$user->hasActivationCode()){
                ActivationCode::createFor($user);
                $user->load('activationCode');
            }

            Mail::queue('emails.activation_code', ['activateCode' => $user->activationCode], function ($m) use ($email) {
                $m->to($email);
            });
        } else if($request->has('mobile')){
            $user = User::findOrCreateWithMobile($mobile);
            if(!$user->hasActivationCode()){
                ActivationCode::createFor($user);
                $user->load('activationCode');
            }
            $this->dispatch(new SendSms($mobile,$user->activationCode->short_code));
        }
        return ['result' => 'OK', 'message' => ''];
    }
}
