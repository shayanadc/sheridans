<?php

namespace App\Http\Controllers;

use App\Provider;
use App\NekaTopUpEntity;
use Illuminate\Http\Request;

use App\Http\Requests;

class NekaTopUpEntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topUps = NekaTopUpEntity::all();
        return [
            'result' => 'ok',
            'message' => 'all topup services',
            'topUps' => $topUps->toArray()
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function visible()
    {
        $topUps = NekaTopUpEntity::visible()->get();
        return [
            'result' => 'ok',
            'message' => 'all topup services',
            'topUps' => $topUps->toArray(),
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider = Provider::findOrFail($request->input('provider_id'));
        $topUp = NekaTopUpEntity::create($request->all());
        $topUp = NekaTopUpEntity::find($topUp->id);
        return[
            'result' => 'ok',
            'message' => 'topup ' . $topUp->id . ' has been saved',
            'topup' => $topUp->toArray()
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topUp = NekaTopUpEntity::findOrFail($id);
        return [
            'result' => 'ok',
            'message' => 'topup' . $topUp->id .'returned',
            'topUp' => $topUp->toArray(),
            'provider' => $topUp->provider->toArray()
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
