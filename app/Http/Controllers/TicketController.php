<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\TicketCode;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Authorizer;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ticket = Ticket::opened()->get();
        return [
            'result' => 'OK',
            'message' => 'all tickets',
            'tickets' => $ticket
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Authorizer::getResourceOwnerId();
        $user = User::findOrFail($userId);
        if($user) {
            $ticketCode = TicketCode::findOrFail($request->input('err_code'));
            $ticket = Ticket::add($user, $ticketCode);
            return [
                'result' => 'OK',
                'message' => '',
                'ticket' => $ticket->toArray()
            ];
        }
        return [
            'result' => 'FAIL',
            'message' => '',
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::findOrFail($id);
        $ticket->close();
            $ticket = Ticket::find($id);
        return [
            'result' => 'OK',
            'message' => 'ticket ' . $ticket->id . ' has been updated',
            'ticket' => $ticket
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
