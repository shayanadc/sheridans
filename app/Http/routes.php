<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('auth/activate', 'ActivationAuthController@auth');
Route::post('access', 'OauthController@access');
Route::get('ticketcodes/visible', 'TicketCodeController@visible');
Route::get('products/{productType}/{phone?}', 'ProductController@getAllProductsWith');
Route::get('products/{productType}/statement/{phone}', 'ProductController@getStatement');
Route::get('gateways/{gwid}/order/{orderId}', 'PaymentGatewayController@getGwParamsForOrder');
Route::group(['middleware' => 'oauth'], function () {
    Route::get('access/revoke', 'OauthController@revoke');
    Route::get('auth/user', 'UserController@authUser');
    Route::put('auth/user', 'UserController@editProfile');
    Route::get('users/profile', 'UserController@owner');
    Route::get('orders/unpaid/{phone?}/{number?}', 'OrderController@unpaid');
    Route::resource('orders', 'OrderController');
    Route::get('payments/user', 'PaymentController@userPayments');
    Route::resource('payments', 'PaymentController');
    Route::resource('ticketcodes', 'TicketCodeController');
    Route::resource('tickets', 'TicketController');
});
Route::get('products/{productType}/statement/{phone}', 'ProductController@getStatement');
Route::get('providers/cities/{id}/types/{pid?}', 'ProviderController@getProviders');
Route::resource('providers', 'ProviderController');
Route::resource('gateways', 'PaymentGatewayController');
Route::resource('cities', 'CityController');
Route::get('provinces/visible', 'ProvinceController@visible');
Route::resource('provinces', 'ProvinceController');
Route::get('topups/visible', 'NekaTopUpEntityController@visible');
Route::resource('topups', 'NekaTopUpEntityController');
Route::resource('users', 'UserController');

Route::group(['prefix' => 'ussd/ik/1.0'], function () {
    Route::get('services/{phone}/{lang}/{mobile}', 'IranKishUSSDController@getServicesFor');
    Route::get('services/cost/{shortCode}/{lang}/{mobile}', 'IranKishUSSDController@getServiceCostFor');
    Route::post('callback', 'IranKishUSSDController@callbackHandler');
});