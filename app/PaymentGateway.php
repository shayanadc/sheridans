<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentGateway extends Model
{
    protected $fillable = ['title','mid','username','password','fallback', 'enabled', 'acno', 'class_name'];
    protected $casts = [
        'fallback' => 'boolean',
        'enabled' => 'boolean'
    ];
    public function productClasses(){
        return $this->belongsToMany('App\ProductClass');
    }
    public function payments() {
        return $this->hasMany('App\Payment', 'gw');
    }
    public function scopeEnabled($query)
    {
        return $query->where('enabled', true);
    }
    public function scopeNoFallback($query)
    {
        return $query->where('fallback', false);
    }
    public function scopeWithFallback($query)
    {
        return $query->where('fallback', true);
    }
    public static function getFallbackGateways()
    {
        return static::where('fallback', true)->enabled()->get();
    }
}
