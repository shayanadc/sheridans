<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class OrderDetail extends Model
{
    protected $casts = [
        'qty' => 'integer',
    ];
    public function phone()
    {
        return $this->belongsTo('App\Phone');
    }
    public function assignee()
    {
        return $this->morphTo();
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function provider(){
        return $this->belongsTo('App\Provider');
    }
    public static function makeFor(Product $product, $qty = 1, Phone $phone = null)
    {
        $orderDetail = new static();
        $orderDetail->product_id = $product->id;
        $orderDetail->provider_id = $product->provider_id;
        $orderDetail->unit_price = $product->price;
        $orderDetail->phone_id = !is_null($phone) ? $phone->id : null;
        $orderDetail->qty = $qty;
        return $orderDetail;
    }

    public static function fromRows(array $rows)
    {
        $details = new Collection();
        foreach ($rows as $row) {
            $qty = isset($row['qty'])? $row['qty'] : 1;
            $phone = isset($row['params']['phone'])? $row['params']['phone'] : null;
            $details->push(static::makeFor($row['product'], $qty, $phone));
        }
        return $details;
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function getSubTotalAttribute()
    {
        return $this->qty * $this->unit_price;
    }
    public function process(){
        $this->product->entity->process($this, $this->params);
    }
    public function registerTransaction($tranactionCode){
        $this->remarks = $tranactionCode;
        $this->processed = true;
        $this->save();
        return $this;
    }

}