<?php

namespace App;
use App\Jobs\PurchasePartaak;
use Illuminate\Foundation\Bus\DispatchesJobs;

class PartaakCRM
{
    use QueueSafely;
    use DispatchesJobs;

    public function process(OrderDetail $detail, $params = null){
        if($this->safeToQueue($detail)){
            $this->dispatch(new PurchasePartaak($detail,$params));
        }else{
            throw new \Exception('order ' . $detail->id . 'not allowed to dispatch in queue');
        }
    }
    public function performSale(OrderDetail $detail, $params = null)
    {
        if (!$detail->processed) {
            //change detail phone numb to json params
            $landLine = $detail->phone->number;
            //
            $partaakP = PartaakProduct::find($detail->product->id);
            $chargeAccount = PartaakAPI::setSales($landLine, $partaakP->product_id);
            if ($chargeAccount->code == "1") {
                $payId = $chargeAccount->result->requestid;
                return $detail->registerTransaction($payId);
            }
            return false;
        }
    }
    public static function allowedTraffic($phone){
        $products = PartaakAPI::getTrafficListForSales($phone);
        foreach($products->result as $product){
            $productIds[] = $product->serviceid;
        }
        return $partakProducts = PartaakProduct::whereIn('product_id', $productIds)->visible()->traffic()->get();
    }
    public static function allowedRenew($phone){
        $product = self::currentService($phone);
        $products = PartaakProduct::where('rate', $product->rate)->where('vol', $product->vol)->get();
        return $products;
    }
    public static function allowedService($phone){
        if(!self::hasReserve($phone)){
            $currentService = PartaakAPI::getCustomerInfo($phone)->result->serviceid;
            $partaakP = PartaakProduct::where('product_id', $currentService)->first();
            return self::availableServices($partaakP->product_id);
        }
    }
    public static function hasReserve($phone){
        $customer = PartaakAPI::getCustomerInfo($phone);
        if(isset($customer->result->servicereserve)){
            return true;
        }else{
            return false;
        }
    }
        public static function currentService($phone){
        $partaakPId = PartaakAPI::getCustomerInfo($phone)->result->serviceid;
        return PartaakProduct::where('product_id', $partaakPId)->first();
    }
    public static function availableServices($partaakId){
        if(PartaakProduct::where('product_id', $partaakId)->greaterThan2Mbps()->first()){
            return PartaakProduct::service()->visible()->get();
        } else {
            return PartaakProduct::lessThan2Mbps()->service()->visible()->get();
        }
    }
    public static function getVolRemaining($phone){
        $result = PartaakAPI::getCustomerInfo($phone)->result;
        $totalVol = $result->remaincredit + $result->deposit;
        return (int)($totalVol);
    }
    public static function getDaysRemaining($phone){
        $result = PartaakAPI::getCustomerInfo($phone)->result;
        return (int)($result->remaindays);
    }

}
