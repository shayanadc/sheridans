<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\PayamResan;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSms extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $mobile;
    protected $message;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mobile, $message)
    {
        $this->mobile = $mobile;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PayamResan $payamResan)
    {
        $payamResan->sendMessage($this->mobile, $this->message);
    }
}
