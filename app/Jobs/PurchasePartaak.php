<?php

namespace App\Jobs;
use App\Jobs\Job;
use App\PartaakCRM;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchasePartaak extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $params;
    protected $details;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details, $params = null)
    {
        $this->params = $params;
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PartaakCRM $CRM)
    {
        $CRM->performSale($this->details, $this->params);
    }
}
