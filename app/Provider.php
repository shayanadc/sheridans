<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;

class Provider extends Model
{
    protected $fillable = ['title','name', 'city_id'];

    protected $casts = [
        'id' => 'integer',
        'city_id' => 'integer',
    ];
    public function cities()
    {
        return $this->belongsToMany('App\City');
    }
    public function products(){
        $productClasses = ProductClass::all();
        $collection = collect([]);
        foreach($productClasses as $type){
            $className = $type->name;
            $m = $className::where('provider_id', $this->id)->get();
            $collection = $collection->merge($m);
        }
        return $collection;
    }
    public function orderDetails(){
        return $this->hasMany('App\OrderDetail');
    }
}
