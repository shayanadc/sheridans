<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name', 'province_id'];
    protected $casts = [
        'province_id' => 'integer',
    ];
    public function province(){
        return $this->belongsTo('App\Province');
    }
    public function providers()
    {
        return $this->belongsToMany('App\Provider');
    }

}
