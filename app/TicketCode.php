<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCode extends Model
{
    protected $fillable = ['title','description', 'visible'];
    protected $casts = [
        'visible' =>'boolean'
    ];

    public function scopeVisible($query){
        $query->where('visible', true);
    }
    public function scopeInVisible($query){
        $query->where('visible', false);
    }
}
