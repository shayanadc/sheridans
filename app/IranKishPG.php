<?php
/**
 * Created by PhpStorm.
 * User: faramarz
 * Date: 20/01/16
 * Time: 12:23
 */

namespace App;

use App\Exceptions\PGCallBackException;
use App\Exceptions\PGPaymentCancelledByUserException;
use App\Exceptions\PGVerificationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SoapClient;
use Log;

class IranKishPG
{
    protected static $dispatch = 'irankish.dispatch';
    protected static $gatewayUrl = 'https://ikc.shaparak.ir/TPayment/Payment/index';
    private static $gatewayName = 'ایران کیش';

    protected $mid;
    protected $pass;
    protected $callback;

    public function __construct(PaymentGateway $paymentGateway) {
        $this->mid = $paymentGateway->mid;
        $this->pass = $paymentGateway->password;
        $this->callback = 'payment_callback/' . $paymentGateway->id;
    }

    public static function dispatchView()
    {
        return static::$dispatch;
    }

    public function getCallbackUrl()
    {
        return url($this->callback);
    }

    public static function getGatewayUrl()
    {
        return static::$gatewayUrl;
    }

    public function getMid()
    {
        return $this->mid;
    }

    public static function getGatewayName()
    {
        return static::$gatewayName;
    }

    protected function getToken($amount, $invoiceNum)
    {
        Log::info('Irankish, soap call');
        $contextOptions = ['ssl' => [
            'verify_peer'      => false,
            'verify_peer_name' => false,
        ]];

        $stream_context = stream_context_create($contextOptions);

        $options = [
            'soap_version'   => SOAP_1_1,
            'stream_context' => $stream_context,
        ];

        $client = new SoapClient('https://ikc.shaparak.ir/XToken/Tokens.xml', $options);
        $params['amount'] = (string)$amount;
        $params['merchantId'] = $this->mid;
        $params['paymentId'] = (string)$invoiceNum;
        $params['invoiceNo'] = (string)$invoiceNum;
        $params['revertURL'] = url($this->callback);
        $result = $client->__soapCall("MakeToken", array($params));
        Log::info('Token call', compact('params', 'result'));

        return $result->MakeTokenResult->token;
    }

    public function getRequestParamsFor(Order $order)
    {
        return [
            'url'  => static::$gatewayUrl,
            'data' => [
                'token'      => $this->getToken($order->total_amount, $order->id),
                'merchantId' => $this->mid,
            ],
        ];
    }

    public function callbackHandler(Request $request)
    {
        $payment = $this->getPayment($request);

        Log::info('Payment: ', compact('payment'));
        if ($payment->state == "100") {
            $payment = $this->verify($payment);
        } else {
            throw new PGCallBackException();
        }

        return $payment;
    }

    /**
     * @param $payment
     * @throws PGVerificationException
     */
    public function verify(\app\OnlinePayment $payment)
    {
        Log::info('Irankish, calling verify');

        $contextOptions = ['ssl' => [
            'verify_peer'      => false,
            'verify_peer_name' => false,
        ]];

        $stream_context = stream_context_create($contextOptions);

        $options = [
            'soap_version'   => SOAP_1_1,
            'stream_context' => $stream_context,
        ];

        $client = new SoapClient('https://ikc.shaparak.ir/XVerify/Verify.xml', $options);
        $params['token'] = $payment->refnum;
        $params['merchantId'] = $this->mid;
        $params['referenceNumber'] = $payment->traceno;
        $params['sha1Key'] = sha1($this->pass);
        $result = $client->__soapCall("KicccPaymentsVerification", array($params));

        Log::info('verify call', compact('result', 'params'));

        if ($result->KicccPaymentsVerificationResult <= 0) {
            throw new PGVerificationException();
        } else {
            $payment->amount = $result->KicccPaymentsVerificationResult;
            $payment->refnum = $payment->resnum;
        }

        return $payment;
    }

    /**
     * @param Request $request
     * @return OnlinePayment
     */
    public function getPayment(Request $request)
    {
        $payment = new OnlinePayment();


        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'merchantId' => 'required',
            'resultCode' => 'required',
            'paymentId' => 'required',
            'referenceId' => 'required',
        ]);

        Log::info('callback input: ', ['Request' => $request->all()]);
        if ($validator->fails()) {
            Log::info('Failed : ', ['Validator' => $validator->errors()->all()]);
            throw new \InvalidArgumentException(trans('messages.pgCallBackException'));
        }

        $payment->state = $request->input('resultCode');
        $payment->refnum = $request->input('token');
        $payment->resnum = $request->input('paymentId');
        $payment->traceno = $request->input('referenceId');
        return $payment;
    }
}