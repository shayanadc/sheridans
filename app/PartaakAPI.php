<?php

namespace App;

use SoapClient;

class PartaakAPI
{
    public static function getCustomerInfo($phone)
    {
        $client = new SoapClient('http://2.181.32.12/partnerservice/CrmService.php?wsdl', ['trace' => 1]);
        $params = ['username' => env('PARTAK_USERNAME'), 'password' => env('PARTAK_PASSWORD'), 'adsltel' => $phone];
        $result = json_decode($client->__call('Get_Customer_Info', $params));
        return $result;
    }

    public static function getServiceListForSales($phone)
    {
        $client = new SoapClient('http://2.181.32.12/partnerservice/CrmService.php?wsdl', ['trace' => 1]);
        $params = ['username' => env('PARTAK_USERNAME'), 'password' => env('PARTAK_PASSWORD'), 'adsltel' => $phone];
        $result = json_decode($client->__call('Get_Service_List_For_Sales', $params));
        return $result;
    }

    public static function getTrafficListForSales($phone)
    {
        $client = new SoapClient('http://2.181.32.12/partnerservice/CrmService.php?wsdl', ['trace' => 1]);
        $params = ['username' => env('PARTAK_USERNAME'), 'password' => env('PARTAK_PASSWORD'), 'adsltel' => $phone];
        $result = json_decode($client->__call('Get_Trafik_List_For_Sales', $params));
        return $result;
    }

    public static function calcServicePayPrice($serviceId)
    {
        $client = new SoapClient('http://2.181.32.12/partnerservice/CrmService.php?wsdl', ['trace' => 1]);
        $params = ['username' => env('PARTAK_USERNAME'), 'password' => env('PARTAK_PASSWORD'), 'serviceid' => $serviceId];
        $result = json_decode($client->__call('Calc_Service_Pay_Price', $params));
        return $result;
    }

    public static function setSales($phone, $serviceId)
    {
        $client = new SoapClient('http://2.181.32.12/partnerservice/CrmService.php?wsdl', ['trace' => 1]);
        $params = ['username' => env('PARTAK_USERNAME'), 'password' => env('PARTAK_PASSWORD'),
            'adsltel' => $phone, 'serviceid' => $serviceId];
        $result = json_decode($client->__call('Set_Sales', $params));
        return $result;
    }
    public static function credit(){
        $client = new SoapClient('http://2.181.32.12/partnerservice/CrmService.php?wsdl', ['trace' => 1]);
        $params = ['username' => env('PARTAK_USERNAME'), 'password' => env('PARTAK_PASSWORD')];
        $result = json_decode($client->__call('Get_Panel_Etebar', $params));
        return $result;
    }
}