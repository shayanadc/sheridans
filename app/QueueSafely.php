<?php
/**
 * Created by PhpStorm.
 * User: shayan
 * Date: 4/26/16
 * Time: 5:27 PM
 */

namespace App;


trait QueueSafely
{
    public function safeToQueue(OrderDetail $detail){
        return (! $detail->queued) && (! $detail->processed) && $detail->product->entity->queueable();
    }

}