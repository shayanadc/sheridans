<?php

use App\ActivationCode;
use App\ActivationCodeVerifier;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivationCodeVerifierTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_user_id_for_mobile_and_activation_short_code()
    {
        $ac = factory(ActivationCode::class)->create();

        $verifier = new ActivationCodeVerifier;

        $this->assertEquals($ac->user->id, $verifier->verify($ac->user->mobile, $ac->short_code));
    }

    /**
     * @test
     */
    public function it_returns_false_for_invalid_activation_short_code()
    {
        $ac = factory(ActivationCode::class)->create();

        $verifier = new ActivationCodeVerifier;

        $this->assertFalse($verifier->verify($ac->user->mobile, str_random(6)));
    }

    /**
     * @test
     */
    public function it_returns_false_for_invalid_activation_long_code()
    {
        $ac = factory(ActivationCode::class)->create();

        $verifier = new ActivationCodeVerifier;

        $this->assertFalse($verifier->verify($ac->user->mobile, str_random(40)));
    }

    /**
     * @test
     */
    public function it_returns_user_id_for_email_and_activation_short_code()
    {
        $ac = factory(ActivationCode::class)->create();

        $verifier = new ActivationCodeVerifier;

        $this->assertEquals($ac->user->id, $verifier->verify($ac->user->email, $ac->short_code));
    }

    /**
     * @test
     */
    public function it_returns_user_id_for_email_and_activation_long_code()
    {
        $ac = factory(ActivationCode::class)->create();

        $verifier = new ActivationCodeVerifier;

        $this->assertEquals($ac->user->id, $verifier->verify($ac->user->email, $ac->long_code));
    }

    /**
     * @test
     */
    public function it_deletes_activation_code_after_successful_verification()
    {
        $ac = factory(ActivationCode::class)->create();

        $verifier = new ActivationCodeVerifier;

        $this->assertEquals($ac->user->id, $verifier->verify($ac->user->email, $ac->long_code));

        $this->dontSeeInDatabase('activation_codes', ['user_id' => $ac->user->id]);
    }
}
