<?php

use App\OrderDetail;
use App\ProductEntity;
use App\ProductSize;
use App\NekaTopUpEntity;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderDetailTest extends TestCase
{
    use DatabaseMigrations;

    private $product;
    /**
     * @test
     */
    public function it_returns_instance_from_given_cart_row_info()
    {
        $this->createTestProduct();
        $orderDetail = OrderDetail::makeFor($this->product, 3);

        $this->assertInstanceOf(OrderDetail::class, $orderDetail);
        $this->assertEquals($this->product->id, $orderDetail->product_id);
        $this->assertEquals($this->product->provider_id, $orderDetail->provider_id);
        $this->assertEquals(3, $orderDetail->qty);

        return $orderDetail;
    }

    /**
     * @test
     * @depends it_returns_instance_from_given_cart_row_info
     */
    public function it_finds_unit_price_of_product($orderDetail)
    {
        $this->createTestProduct();
        $orderDetail = OrderDetail::makeFor($this->product, 3);

        $this->assertEquals($this->product->price, $orderDetail->unit_price);
    }

    /**
     * @test
     * @depends it_returns_instance_from_given_cart_row_info
     */
    public function it_calculates_the_subtotal($orderDetail)
    {
        $this->createTestProduct();
        $orderDetail = OrderDetail::makeFor($this->product, 3);

        $this->assertEquals(3 * $this->product->price, $orderDetail->subTotal);
    }

    /**
     * @test
     */
    public function it_saves_the_order_detail_with_size_title()
    {
        $this->createTestProduct();

        $orderDetail = OrderDetail::makeFor($this->product, 3);

        $order = factory(\App\Order::class)->create();
        $order->orderDetails()->save($orderDetail);
        $this->seeInDatabase('order_details', [
            'order_id'     => $order->id,
            'product_id'   => $this->product->entity_id,
            'provider_id'  => $this->product->provider_id,
            'qty'          => 3,
            'unit_price'   => $this->product->price,
        ]);
    }
    
    private function createTestProduct()
    {
        $this->product = factory(\App\Product::class)->create();
    }

}