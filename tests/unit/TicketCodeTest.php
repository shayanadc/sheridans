<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\TicketCode;
class TicketCodeTest extends TestCase
{
    use DatabaseMigrations;
  /**
   * @test
   */
    public function it_save_error_code_with_necessary_item(){
        $data= [
            'title' => 'fail',
            'description' => 'fa...',
            'visible' => true
        ];
        $ticket = TicketCode::create($data);
        $this->seeInDatabase('ticket_codes',$data);
    }
    /**
     * @test
     */
    public function its_visible_scope_returns_instances() {
        $ticket = factory(TicketCode::class)->create();
        $this->assertCount(1, TicketCode::all());

        $ticket->visible = false;
        $ticket->save();

        $this->assertCount(0, TicketCode::visible()->get());
        $this->assertCount(1, TicketCode::inVisible()->get());
    }
}
