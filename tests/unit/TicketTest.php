<?php

use App\User;
use App\TicketCode;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Ticket;
class TicketTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_saves_ticket_for_user_with_necessary_item(){
        $user = factory(User::class)->create();
        $code = factory(TicketCode::class)->create();
        $ticket = Ticket::add($user, $code);
        $data= [
            'user_id' => $user->id,
            'err_code' => $code->id,
            'closed' => false
        ];
        $this->seeInDatabase('tickets',$data);
    }
    /**
     * @test
     */
    public function it_closed_ticket_that_opened(){
        $ticket = factory(Ticket::class)->create();
        $ticket->closed = false;
        $this->assertFalse($ticket->closed);
        $ticket->close();
        $this->assertTrue($ticket->closed);
    }

    /**
     * @test
     */
    public function its_open_scope_returns_instances() {
        $ticket = factory(Ticket::class)->create(['closed' => false]);
        $this->assertCount(1, Ticket::all());

        $this->assertCount(0, Ticket::closed()->get());
        $this->assertCount(1, Ticket::opened()->get());
    }
}
