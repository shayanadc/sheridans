<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProviderTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_saves_provider_for_specific_city(){
        $city = factory(\App\City::class)->create();
        $provider = factory(App\Provider::class)->create();
        $data= [
            'title' => 'adsl',
            'name' => 'miaad',
        ];
        $pr = App\Provider::create($data);
        $this->seeInDatabase('providers',$data);
    }
}