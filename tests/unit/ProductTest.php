<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_is_an_instance_of_product(){

        factory(\App\NekaTopUpEntity::class, 4)->create(['operator' => 'mtn', 'visible' => true]);
        factory(\App\NekaTopUpEntity::class, 3)->create(['operator' => 'mci']);
        factory(\App\NekaTopUpEntity::class, 2)->create(['operator' => 'mtn', 'visible' => false]);
        $salesP = \App\NekaTopUpEntity::getSalesProducts('09353993219');
        $this->assertCount(4, $salesP);

    }
}
