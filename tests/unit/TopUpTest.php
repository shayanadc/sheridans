<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\NekaTopUpEntity;
use App\Product;
class NekaTopUpEntityTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_is_an_instance_of_product(){
        $t = factory(NekaTopUpEntity::class)->create();
        $this->assertInstanceOf(\App\ProductEntity::class, $t);
    }
}
