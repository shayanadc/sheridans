<?php

use App\Order;
use App\OrderDetail;
use App\PartaakProduct;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartaakCRMTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_adds_a_job_to_partaak_queue()
    {
        $product = factory(\App\Product::class)->create();
        $order = factory(Order::class)
            ->create()
            ->orderDetails()->save(
                factory(OrderDetail::class)->make([
                    'product_id' => $product->id,
                ])
            );
        $this->expectsJobs(App\Jobs\PurchasePartaak::class);
        $order->process();
    }
}
