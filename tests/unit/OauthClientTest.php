<?php

use App\OauthClient;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OauthClientTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function its_public_incrementing_attribute_is_false()
    {
        $oc = factory(OauthClient::class)->make();
        $this->assertFalse($oc->incrementing);
    }
}
