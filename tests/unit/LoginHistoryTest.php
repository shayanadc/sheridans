<?php

use App\LoginHistory;
use App\OauthClient;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginHistoryTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_creates_a_new_login_history_for_the_given_user_and_client()
    {
        $user = factory(User::class)->create();
        $client = factory(OauthClient::class)->create();

        $this->dontSeeInDatabase('login_histories', [
            'user_id' => $user->id,
            'oauth_client_id' => $client->id
        ]);

        $lh = LoginHistory::register($user, $client);

        $this->assertInstanceOf(LoginHistory::class, $lh);
        $this->assertEquals($user->id, $lh->user_id);
        $this->assertEquals($client->id, $lh->oauth_client_id);

        $this->seeInDatabase('login_histories', [
            'user_id' => $user->id,
            'oauth_client_id' => $client->id
        ]);
    }

    /**
     * @test
     */
    public function it_updates_an_existing_login_history_for_the_given_user_and_client()
    {
        $past = Carbon::now()->subDay();
        $lh  = factory(LoginHistory::class)->create([
            'created_at' => $past,
            'updated_at' => $past
        ]);

        $user = User::find($lh->user_id);
        $client = OauthClient::find($lh->oauth_client_id);

        $this->assertTrue(Carbon::now()->gt($lh->created_at));

        $ulh = LoginHistory::register($user, $client);

        $this->assertEquals($lh->created_at, $ulh->created_at);
        $this->assertTrue($ulh->updated_at->gt($lh->updated_at));
    }
}