<?php

use App\User;
use App\ActivationCode;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    /**
     * @test
     */
    function it_has_a_nullable_email_address()
    {
        $u = factory(User::class)->create(['email' => null]);

        $this->assertNull($u->email);

        $u = factory(User::class)->create(['email' => 'test@example.com']);

        $this->assertEquals('test@example.com', $u->email);
    }
    /**
     * @test
     */
    function it_has_a_nullable_mobile_number()
    {
        $u = factory('App\User')->create(['mobile' => null]);

        $this->assertNull($u->mobile);

        $u = factory(User::class)->create(['mobile' => '09121234567']);

        $this->assertEquals('09121234567', $u->mobile);
    }
    /**
    * @test
    */
    function it_finds_existing_user_with_email(){
        $u = factory('App\User')->create(['email' => 'sony@example.com']);
        $user = User::findOrCreateWithEmail('sony@example.com');
        $this->assertEquals($u->id, $user->id);
    }
    /**
     * @test
     */
    function it_finds_existing_user_with_mobile(){
        $u = factory('App\User')->create(['mobile' => '09112718982']);
        $user = User::findOrCreateWithMobile('09112718982');
        $this->assertEquals($u->id, $user->id);
    }
    /**
     * @test
     */
    function it_create_not_existing_user_with_email(){
        $this->notSeeInDatabase('users', ['email' => 'sony@example.com']);
        $user = User::findOrCreateWithEmail('sony@example.com');
        $u = User::where('email','sony@example.com')->first();
        $this->seeInDatabase('users', ['email' => 'sony@example.com']);
        $this->assertEquals($u->id, $user->id);
    }
    /**
     * @test
     */
    function it_create_not_existing_user_with_mobile(){
        $this->notSeeInDatabase('users', ['mobile' => '09112718982']);
        $user = User::findOrCreateWithMobile('09112718982');
        $u = User::where('mobile','09112718982')->first();
        $this->seeInDatabase('users', ['mobile' => '09112718982']);
        $this->assertEquals($u->id, $user->id);
    }
    /**
     * @test
     */
    function create_user_with_email(){
        $this->notSeeInDatabase('users', ['email' => 'sony@example.com']);
        $user = User::createWithEmail('sony@example.com');
        $this->assertInternalType('string', $user->username);
        $this->assertInternalType('string', $user->password);
        $this->seeInDatabase('users', ['email' => 'sony@example.com']);
    }
    /**
     * @test
     */
    function create_user_with_mobile(){
        $this->notSeeInDatabase('users', ['mobile' => '09112718982']);
        $user = User::createWithMobile('09112718982');
        $this->assertInternalType('string', $user->username);
        $this->assertInternalType('string', $user->password);
        $this->seeInDatabase('users', ['mobile' => '09112718982']);
    }
    /**
     * @test
     */
    function it_has_no_activation_code()
    {
        $u = factory(User::class)->create();

        $this->assertNull($u->activationCode);
    }

    /**
     * @test
     */
    function it_can_have_one_activation_code()
    {
        $u = factory(User::class)->create();
        $ac = factory(ActivationCode::class)->create(['user_id' => $u->id]);

        $this->assertEquals($ac->id, $u->activationCode->id);
    }

    /**
     * @test
     * @expectedException \Illuminate\Database\QueryException
     */
    function it_can_have_only_one_activation_code()
    {
        $u = factory(User::class)->create();

        $ac1 = factory(ActivationCode::class)->make();
        $ac2 = factory(ActivationCode::class)->make();

        $u->activationCode()->save($ac1);
        $u->activationCode()->save($ac2);
    }

    /**
     * @test
     */
    public function it_has_a_few_constants_containing_role_values()
    {
        $this->assertEquals(1, User::SUPER_ADMIN_ROLE);
        $this->assertEquals(2, User::ADMIN_ROLE);
        $this->assertEquals(3, User::SUPERVISOR_ROLE);
        $this->assertEquals(4, User::SALES_ROLE);
        $this->assertEquals(5, User::ACCOUNTANT_ROLE);
        $this->assertEquals(6, User::SUPPORT_ROLE);
        $this->assertEquals(7, User::CUSTOMER_ROLE);
    }

    /**
     * @test
     */
    public function it_creates_users_with_default_role_of_customer()
    {
        $u = User::create([
            'username' => 'myuser',
            'password' => str_random(),
            'email' => 'email@example.com',
            'givenName' => 'John',
            'surname' => 'Smith',
            'birth_date' => new \Carbon\Carbon('1960-07-10'),
            'gender' => 'm'
        ]);
        $user = User::find($u->id);
        $this->assertEquals(User::CUSTOMER_ROLE, $user->role);
    }
}
