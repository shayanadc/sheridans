<?php

use App\ActivationCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserActivationCodeTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_activation_code_for_the_user()
    {
        $u = factory(User::class)->create();
        $ac = new ActivationCode();
        $u->activationCode()->save($ac);

        $this->assertInstanceOf(ActivationCode::class, $u->activationCode);
        $this->assertEquals($ac->id, $u->activationCode->id);
    }

    /**
     * @test
     */
    public function it_returns_null_for_user_with_expired_activation_code()
    {
        $u = factory(User::class)->create();
        $ac = new ActivationCode();
        $u->activationCode()->save($ac);

        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();

        $this->assertNull($u->activationCode);
    }


    /**
     * @test
     */
    public function it_returns_false_if_user_has_no_activation_code()
    {
        $u = factory(User::class)->create();

        $this->assertFalse($u->hasActivationCode());
    }

    /**
     * @test
     */
    public function it_returns_false_if_user_activation_code_is_expired()
    {
        $u = factory(User::class)->create();
        $ac = new ActivationCode();
        $u->activationCode()->save($ac);

        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();

        $this->assertFalse($u->hasActivationCode());
    }

    /**
     * @test
     */
    public function it_returns_true_if_user_has_activation_code()
    {
        $u = factory(User::class)->create();
        $ac = new ActivationCode();
        $u->activationCode()->save($ac);

        $this->assertTrue($u->hasActivationCode());
    }

    /**
     * @test
     */
    public function it_returns_user_of_an_activation_code()
    {
        $u = factory(User::class)->create();
        $ac = new ActivationCode();
        $u->activationCode()->save($ac);

        $this->assertInstanceOf(User::class, $ac->user);
        $this->assertEquals($u->id, $ac->user->id);
    }


    /**
     * @test
     */
    public function it_creates_a_new_activation_code_for_user()
    {
        $u = factory(User::class)->create();
        $ac = ActivationCode::createFor($u);

        $this->assertInstanceOf(ActivationCode::class, $ac);
        $this->assertEquals($u->id, $ac->user->id);
    }

    /**
     * @test
     */
    public function it_replaces_an_expired_application_code_with_new_one()
    {
        $u = factory(User::class)->create();
        $ac = ActivationCode::createFor($u);
        $ac->created_at = Carbon::now()->subMinutes(30);
        $ac->save();

        $this->assertFalse($u->hasActivationCode());

        $ac = ActivationCode::createFor($u);
        $u->load('activationCode');
        $this->assertTrue($u->hasActivationCode());
    }
}
