<?php

use App\ActivationCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivationCodeTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return ActivationCode
     */
    protected static function createStubActivationCode()
    {
        $ac = new ActivationCode;
        $ac->generateCodes();

        $u = factory(User::class)->create();
        $ac->user_id = $u->id;
        $ac->save();
        return $ac;
    }

    /**
     * @test
     */
    public function it_has_a_long_code() {
        $ac = new ActivationCode();
        $ac->generateCodes();

        $this->assertInternalType('string', $ac->long_code);
        $this->assertEquals(40, strlen($ac->long_code));
    }

    /**
     * @test
     */
    public function it_has_a_short_code() {
        $ac = new ActivationCode();
        $ac->generateCodes();

        $this->assertInternalType('string', $ac->short_code);
        $this->assertEquals(6, strlen($ac->short_code));
        $this->assertTrue(is_numeric($ac->short_code));
    }

    /**
     * @test
     */
    public function it_create_unique_codes() {
        $ac1 = new ActivationCode();
        $ac1->generateCodes();

        $ac2 = new ActivationCode();
        $ac2->generateCodes();

        $this->assertNotEquals($ac1->short_code, $ac2->short_code);
        $this->assertNotEquals($ac1->long_code, $ac2->long_code);
    }

    /**
     * @test
     */
    public function it_has_created_at_field() {
        $ac = self::createStubActivationCode();

        $this->assertInstanceOf(Carbon::class, $ac->created_at);
    }

    /**
     * @test
     */
    public function its_expired_scope_returns_instances_older_than_20_minutes_by_default() {
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(15);
        $ac->save();
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();

        $this->assertCount(2, ActivationCode::all());
        $this->assertCount(1, ActivationCode::expired()->get());
    }

    /**
     * @test
     */
    public function its_not_expired_scope_returns_instances_newer_than_20_minutes_by_default() {
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(15);
        $ac->save();
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();

        $this->assertCount(2, ActivationCode::all());

        $this->assertCount(1, ActivationCode::notExpired()->get());
    }

    /**
     * @test
     */
    public function it_deletes_instances_older_than_20_minutes_by_default() {
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(15);
        $ac->save();
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();

        $this->assertCount(2, ActivationCode::all());

        ActivationCode::deleteExpired();
        $this->assertCount(0, ActivationCode::expired()->get());
    }

    /**
     * @test
     */
    public function its_expired_scope_returns_instances_older_than_given_minutes() {
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(35);
        $ac->save();

        $this->assertCount(2, ActivationCode::all());

        $this->assertCount(1, ActivationCode::expired(30)->get());
    }

    /**
     * @test
     */
    public function its_not_expired_scope_returns_instances_newer_than_given_minutes() {
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(35);
        $ac->save();

        $this->assertCount(2, ActivationCode::all());

        $this->assertCount(1, ActivationCode::notExpired(30)->get());
    }

    /**
     * @test
     */
    public function it_deletes_instances_older_than_given_minutes() {
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(25);
        $ac->save();
        $ac = self::createStubActivationCode();
        $ac->created_at = Carbon::now()->subMinutes(35);
        $ac->save();

        $this->assertCount(2, ActivationCode::all());

        ActivationCode::deleteExpired(30);
        $this->assertCount(0, ActivationCode::expired(30)->get());
    }

    /**
     * @test
     */
    public function it_checks_for_expired_activation_codes_before_deleting() {
        ActivationCode::deleteExpired();
    }
}
