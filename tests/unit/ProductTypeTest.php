<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTypeTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_saves_productType_for(){
        $type = factory(App\ProductType::class)->create(['title' => 'adsl']);
        $this->assertInstanceOf(\App\ProductType::class, $type);
        $this->assertEquals('adsl', $type->title);
    }
}
