<?php

use App\Order;
use App\PaymentGateway;
use App\SystemUser;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_creats_payment_without_order(){
        $user = factory(User::class)->create();
        $order = factory(Order::class)->create();
        $gw = factory(PaymentGateway::class)->create();
        $sysUser = factory(SystemUser::class)->create();
        $data= [
            'amount' => $order->total_amount,
            'gw' => $gw->id,
            'method' => 'online',
            'registrant_id' => $sysUser->id,
            'registrant_type' => get_class($sysUser)
        ];
        $payment = App\Payment::create($data);
        $this->seeInDatabase('payments',$data);
    }
}
