<?php

use App\Order;
use App\OrderDetail;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_creates_an_order_for_a_collection_of_chosen_products()
    {
        $user = factory(User::class)->create();

        $details = collect([
            factory(OrderDetail::class)->make(['qty' => 2, 'unit_price' => 3.50]),
            factory(OrderDetail::class)->make(['qty' => 3, 'unit_price' => 2.00])
        ]);

        $order = Order::createFromDetails($details, $user);

        $this->assertInstanceOf(Order::class, $order);
        $this->assertCount(2, $order->orderDetails);

        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $order->orderDetails);
        $this->assertContainsOnlyInstancesOf(OrderDetail::class, $order->orderDetails);

        return $order;
    }

    /**
     * @test
     * @depends it_creates_an_order_for_a_collection_of_chosen_products
     */
    public function it_returns_the_date_of_the_created_order($order)
    {
        $this->assertEquals($order->created_at, $order->date);
    }

    /**
     * @test
     * @depends it_creates_an_order_for_a_collection_of_chosen_products
     */
    public function it_checks_the_total_qty_of_the_created_order($order)
    {
        $this->assertEquals(5, $order->total_qty);
    }

    /**
     * @test
     * @depends it_creates_an_order_for_a_collection_of_chosen_products
     */
    public function it_checks_the_total_amount_of_the_created_order($order)
    {
        $this->assertEquals(13.00, $order->total_amount);
    }
}