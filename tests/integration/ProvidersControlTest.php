<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProvidersControlTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_all_provider_with_cities(){
        $provider1 = factory(\App\Provider::class)->create();
        $city1 = factory(\App\City::class)->create();
        $city2 = factory(\App\City::class)->create();
        $provider1->cities()->attach([$city1->id,$city2->id]);
        $providers = \App\Provider::with('cities')->get();
        $this->get('/providers')
            ->seeJson([
                'result' => 'ok',
                'message' => 'all providers with cities',
                'providers' => $providers->toArray()
            ]);
    }
    /**
     * @test
     */
    public function it_returns_cities_of_specific_provider(){
        $provider1 = factory(\App\Provider::class)->create();
        $city1 = factory(\App\City::class)->create();
        $city2 = factory(\App\City::class)->create();
        $provider1->cities()->attach([$city1->id,$city2->id]);
        $this->get('/providers/'. $provider1->id);
        $provider1 = \App\Provider::with('cities')->find($provider1->id);
            $this->seeJson([
                'result' => 'ok',
                'message' => 'cities of provider',
                'provider' => $provider1->toArray(),
                'cities' => $provider1->cities->toArray()
            ]);
    }
    /**
     * @test
     */
    public function it_saves_provider_with_cities(){
        $city1 = factory(\App\City::class)->create();
        $city2 = factory(\App\City::class)->create();
        $data = [
            'title' => 'adsl',
            'name' => str_random(5),
            'cities_id' => [$city1->id, $city2->id]
        ];
        $this->post('/providers', $data);
        $provider = \App\Provider::first();
//        $cities = \App\City::with('providers')->get();
//        dd($cities->toArray(), $provider->cities->toArray());
//        $this->assertEquals($cities->toArray(), $provider->cities->toArray());
        $this->seeJson([
            'result' => 'ok',
            'message' => 'provider has been updated',
            'provider' => $provider->toArray(),
            'cities' => $provider->cities->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_returns_providers_for_cities(){
        $city1 = factory(\App\City::class)->create();
        $city2 = factory(\App\City::class)->create();
        $provider1 = factory(\App\Provider::class)->create();
        $provider2 = factory(\App\Provider::class)->create();
        $provider3 = factory(\App\Provider::class)->create();
        $provider1->cities()->sync([
           $city1->id,
           $city2->id
        ]);
        $provider2->cities()->sync([
            $city1->id,
        ]);
        $this->get('/providers/cities/'. $city1->id . '/types');
        $providers = $city1->providers;
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'providers' => $providers->toArray(),
        ]);
    }
    /**
     * @test
     */
    public function it_returns_providers_for_specific_city_and_product_type(){
        $city1 = factory(\App\City::class)->create();
        $city2 = factory(\App\City::class)->create();
        $provider1 = factory(\App\Provider::class)->create(['title' => 'adsl']);
        $provider2 = factory(\App\Provider::class)->create(['title' => 'topup']);
        $provider3 = factory(\App\Provider::class)->create(['title' => 'topup']);
        $provider1->cities()->sync([
            $city1->id,
            $city2->id
        ]);
        $provider2->cities()->sync([
            $city1->id,
        ]);
        $this->get('/providers/cities/'. $city1->id . '/types/adsl');
        $providers = $city1->providers()->where('title', 'adsl')->get();
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'providers' => $providers->toArray(),
        ]);
    }
}
