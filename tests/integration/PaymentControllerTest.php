<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_error_without_a_valid_access_token()
    {
        $this->post('/payments')
            ->seeJson(['error' => 'invalid_request']);
    }
    /**
     * @test
     */
    public function it_save_payment_with_valid_data(){
        $this->withoutMiddleware();
        $gw = factory(\App\PaymentGateway::class)->create();
        $supporter = factory(\App\User::class)->create(['role' => \App\User::SUPPORT_ROLE]);
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($supporter->id);
        $order = factory(\App\Order::class)->create();
        $this->post('/payments', [
            'order_id' => $order->id,
            'gw' => $gw->id,
            'pan' => '123',
            'tracking_code' => '321',
            'receipt_number' => '321'
        ]);
        $payment = \App\Payment::first();
        $this->seeJson([
            'result' => 'OK',
            'message' => 'payment' . $payment->id .'returned',
            'payment' => $payment->toArray(),
        ]);
    }
    /**
     * @test
     */
    public function it_returns_specific_payment(){
        $this->withoutMiddleware();
        $order = factory(\App\Order::class)->create();
        factory(\App\Payment::class)->create(['order_id' => $order->id]);
        $payment = \App\Payment::first();
//        $this->assertEquals($order->toArray(), $payment->order->toArray());
        $this->get('/payments/' . $payment->id)->seeJson([
            'result' => 'OK',
            'message' => 'payment' . $payment->id .'returned',
            'payment' => $payment->toArray(),
            'order' => $payment->order->toArray()
            ]);
    }
    /**
     * @test
     */
    public function it_updates_specific_payment(){
        $this->withoutMiddleware();
        factory(\App\Payment::class)->create();
        $payment = \App\Payment::first();
        $this->put('/payments/' . $payment->id, ['tracking_code' => '321', 'confirmed' => 1]);
        $payment = \App\Payment::first();
        $this->assertTrue($payment->confirmed);
        $this->assertEquals('321', $payment->tracking_code);
        $this->seeJson([
            'result' => 'OK',
            'message' => 'payment' . $payment->id .'returned',
            'payment' => $payment->toArray(),
        ]);
    }
    /**
     * @test
     */
    public function it_returns_all_payments_of_auth_user(){
        $this->withoutMiddleware();
        $user = factory(\App\User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);
        $order1 = factory(\App\Order::class)->create(['user_id' => $user->id]);
        $order2 = factory(\App\Order::class)->create(['user_id' => $user->id]);

        $payment1 = factory(\App\Payment::class)->create(['order_id' => $order1->id]);
        $payment2 = factory(\App\Payment::class)->create(['order_id' => $order2->id]);
        $this->get('/payments/user/');
        $user = \App\User::find($user->id);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'payments' => $user->payments->toArray()
        ]);
    }
}
