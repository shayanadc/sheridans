<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersRouteTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_error_message_for_unauthenticated_user()
    {
        $this->get('/users/profile')
            ->assertResponseStatus(400);
        $this->seeJson(['error' => 'invalid_request']);
    }

    /**
     * @test
     */
    public function it_returns_the_current_users_info()
    {
        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);

        $this->get('/users/profile')
            ->seeJson([
                'id' => $user->id,
                'username' => $user->username,
            ]);
    }

    /**
     * @test
     */
    public function it_returns_the_users_info_without_password_and_remember_token()
    {
        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);

        $this->get('/users/profile')
            ->dontSeeJson([
                'password' => $user->password,
                'remember_token' => $user->remember_token,
            ]);
    }
}
