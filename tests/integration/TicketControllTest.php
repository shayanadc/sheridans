<?php

use App\Ticket;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketControllTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_all_opened_ticket()
    {
        $this->withoutMiddleware();
        $openTickets = factory(\App\Ticket::class, 2)->create(['closed' => false]);
        factory(\App\Ticket::class, 3)->create(['closed' => true]);

        $this->get('/tickets')
            ->seeJson([
                'result' => 'OK',
                'message' => 'all tickets',
                'tickets' => $openTickets->toArray()
            ]);
    }

    /**
     * @test
     */
    public function it_save_ticket_for_user()
    {
        $this->withoutMiddleware();
        $ticketCode = factory(\App\TicketCode::class)->create();
        $user = factory(\App\User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);
        $data = [
            'user_id' => $user->id,
            'err_code' => $ticketCode->id,
        ];
        $this->post('/tickets', $data);
        $tc = Ticket::first();
        $this->seeJson([
                'result' => 'OK',
                'message' => '',
                'ticket' => $tc->toArray()
            ]);
    }

    /**
     * @test
     */
    public function it_change_state_ticket_to_closed()
    {
        $this->withoutMiddleware();
        $ticket = factory(\App\Ticket::class)->create(['closed' => false]);
        $this->put('/tickets/' . $ticket->id, ['closed' => true]);
        $ticket = Ticket::find($ticket->id);
        $this->assertTrue($ticket->closed);
        $this->seeJson([
            'result' => 'OK',
            'message' => 'ticket ' . $ticket->id . ' has been updated',
            'ticket' => $ticket->toArray()
        ]);
    }
}
