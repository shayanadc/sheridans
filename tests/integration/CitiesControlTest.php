<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CitiesControlTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_all_cities(){
        factory(\App\City::class, 2)->create();
        $this->get('/cities');
        $validCities = \App\City::with('province')->get();
        $this->assertCount(2, $validCities->toArray());
        $this->seeJson([
            'result' => 'ok',
            'message' => 'all cities with provinces',
            'cities' => $validCities->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_save_new_city_with_valid_data(){
        $province = factory(\App\Province::class)->create();
        $data = $data = [
            'province_id' => $province->id,
            'name' => str_random(4),
        ];
        $this->post('/cities', $data);
        $city = \App\City::where('name', $data['name'])->where('province_id', $province->id)->with('province')->first();
        $this->assertEquals($province->id, $city->province_id);
        $this->seeJson([
            'result' => 'ok',
            'message' => 'city ' . $city->id . ' has been saved',
            'city' => $city->toArray()
        ]);
    }

    /**
     * @test
     */
    public function it_change_province_and_name_of_city()
    {
        $newProvince = factory(\App\Province::class)->create();
        $province = factory(\App\Province::class)->create();
        $city = factory(\App\City::class)->create(['province_id' => $province->id]);
        $this->put('/cities/' . $city->id, ['name' => 'gorg', 'province_id' => $newProvince->id]);
        $city = \App\City::find($city->id);
        $this->assertEquals('gorg', $city->name);
        $this->assertEquals($city->province_id, $newProvince->id);
        $this->assertNotEquals($city->province_id, $province->id);
        $this->seeJson([
            'result' => 'ok',
            'message' => 'city ' . $city->id . ' has been updated',
            'city' => $city->toArray()
        ]);
    }
}