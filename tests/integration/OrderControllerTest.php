<?php

use App\NekaTopUpEntity;
use App\Order;
use App\Phone;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_error_without_a_valid_access_token()
    {
        $this->post('/orders')
            ->seeJson(['error' => 'invalid_request']);
    }

    /**
     * @test
     */
    public function it_returns_json_of_a_new_order_based_on_the_given_values()
    {
        $this->withoutMiddleware();
        $prods = factory(\App\Product::class,3)->create();
        $customer = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($customer->id);
        $pt = \App\ProductClass::first();
        $pg = factory(\App\PaymentGateway::class)->create(['enabled' => true]);
        $pt->paymentGateways()->sync([
            $pg->id
        ]);
        $this->post('/orders', [
            'rows' => [
                ['id' => $prods->get(0)->id],
                ['id' => $prods->get(1)->id, 'qty' => 2],
                ['id' => $prods->get(2)->id, 'qty' => 3],
            ]
        ]);

        $order = Order::first();
        $gw = $pt->paymentGateways;
        $prd = $order->orderDetails->first()->product->entity;
        $this->assertEquals($gw->toArray(), $order->orderDetails->first()->product->entity->pgs()->toArray());
        $this
            ->seeJson([
                'result' => 'OK',
                'message' => '',
                'order' => [
                    'id' => $order->id,
                    'total_amount' => $order->total_amount,
                    'user_id' => $customer->id,
                    'issuer_id' => $customer->id,
                ],
                'gateways' => $order->orderDetails->first()->product->entity->pgs()->toArray()
            ])
        ;
    }

    /**
     * @test
     */
    public function it_returns_json_of_a_new_order_based_on_the_given_values_by_a_user_for_another_user_by_a_supporter()
    {
        $this->withoutMiddleware();
        $prods = factory(\App\Product::class,3)->create();
        $customer = factory(User::class)->create(['role' => User::CUSTOMER_ROLE]);
        $supporter = factory(User::class)->create(['role' => User::SUPPORT_ROLE]);
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($supporter->id);
        $pt = factory(\App\ProductClass::class)->create(['name' => \App\PartaakProduct::class]);

        $this->post('/orders', [
            'customer_id' => $customer->id,
            'rows' => [
                ['id' => $prods->get(0)->id, 'qty' => 1],
                ['id' => $prods->get(1)->id, 'qty' => 2],
                ['id' => $prods->get(2)->id, 'qty' => 3],
            ]
        ]);

        $order = Order::first();

        $this
            ->seeJson([
                'result' => 'OK',
                'message' => '',
                'order' => [
                    'id' => $order->id,
                    'total_amount' => $order->total_amount,
                    'user_id' => $customer->id,
                    'issuer_id' => $supporter->id,
                ],
                'gateways' => $order->orderDetails->first()->product->entity->pgs()->toArray()
            ])
        ;
    }

    /**
     * @test
     */
    public function it_returns_json_of_a_new_order_based_on_the_given_values_by_a_user_for_another_user_by_a_supervisor()
    {
        $this->withoutMiddleware();
        $prods = factory(\App\Product::class,3)->create();
        $customer = factory(User::class)->create(['role' => User::CUSTOMER_ROLE]);
        $supervisor = factory(User::class)->create(['role' => User::SUPERVISOR_ROLE]);
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($supervisor->id);
        $pt = factory(\App\ProductClass::class)->create(['name' => \App\PartaakProduct::class]);

        $this->post('/orders', [
            'customer_id' => $customer->id,
            'rows' => [
                ['id' => $prods->get(0)->id, 'qty' => 1],
                ['id' => $prods->get(1)->id, 'qty' => 2],
                ['id' => $prods->get(2)->id, 'qty' => 3],
            ]
        ]);

        $order = Order::first();

        $this
            ->seeJson([
                'result' => 'OK',
                'message' => '',
                'order' => [
                    'id' => $order->id,
                    'total_amount' => $order->total_amount,
                    'user_id' => $customer->id,
                    'issuer_id' => $supervisor->id,
                ],
                'gateways' => $order->orderDetails->first()->product->entity->pgs()->toArray()
            ])
        ;
    }
    /**
     * @test
     */
    public function it_returns_specific_order(){
        $this->withoutMiddleware();
        $od = factory(\App\OrderDetail::class)->create();
        $order = $od->order;
        $pt = \App\ProductClass::first();
        $gw = \App\PaymentGateway::first();
        $pt->paymentGateways()->sync([$gw->id]);
        $this->get('/orders/'. $order->id);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'order' => $order->toArray(),
            'gateways' => $order->orderDetails->first()->product->entity->pgs()->toArray()
        ]);
    }

    /**
     * @test
     */
    public function it_returns_unpaid_orders_for_phone_number(){
        $this->withoutMiddleware();
        $od = factory(\App\OrderDetail::class)->create();
        $phone = $od->phone;
        $ods = $phone->orderDetails;
        foreach($ods as $od){
            $coll[] = $od->order_id;
        }
        $orders = Order::whereIn('id', $coll)->unpaid()->get();
        $this->get('orders/unpaid/phone/'. $od->phone->number);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'orders' => $orders->toArray()
        ]);
    }

    /**
     * @test
     */
    public function it_returns_unpaid_orders_for_auth_user(){
        $this->withoutMiddleware();
        $user = factory(User::class)->create();
        $customer = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);
        factory(Order::class,3)->create(['user_id' => $user->id]);
        factory(Order::class,3)->create(['user_id' => $customer->id]);
        $ownOrders = Order::where('user_id', $user->id)->unpaid()->get();
        $this->get('orders/unpaid');
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'orders' => $ownOrders->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_returns_null_if_user_has_not_payment(){
        $this->withoutMiddleware();
        $user = factory(User::class)->create();
        $customer = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);
        $order = factory(Order::class)->create(['user_id' => $user->id]);
        factory(\App\Payment::class)->create(['order_id' => $order->id]);
        $ownOrders = Order::where('user_id', $user->id)->unpaid()->get();
        $this->get('orders/unpaid');
        $this->assertEmpty($ownOrders);
    }
    /**
     * @test
     */
    public function it_returns_null_if_phone_has_not_payment(){
        $this->withoutMiddleware();
        $number = '12512515';
        $this->get('orders/unpaid/phone/'. $number);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'orders' => null
        ]);
    }
    /**
     * @test
     */
    public function it_returns_fail_if_send_incorrect_route()
    {
        $this->withoutMiddleware();
        $this->get('orders/unpaid/125415/');
        $this->seeJson([
            'result' => 'FAIL',
            'message' => ''
        ]);
    }
    /**
     * @test
     */
    public function it_returns_json_of_a_new_order_for_phone_number()
    {
        $this->withoutMiddleware();
        $prods = factory(\App\Product::class,3)->create();
        $customer = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($customer->id);
        $pt = factory(\App\ProductClass::class)->create(['name' => \App\PartaakProduct::class]);
        $pg = factory(\App\PaymentGateway::class)->create();
        $pt->paymentGateways()->sync([
            $pg->id,
        ]);
        $this->post('/orders', [
            'rows' => [
                ['id' => $prods->get(0)->id, 'qty' => 1, 'params' => ['land_line' => '1132289982']],
                ['id' => $prods->get(1)->id, 'qty' => 2],
                ['id' => $prods->get(2)->   id, 'qty' => 3],
            ]
        ]);
        $order = Order::first();
        $user = $order->user;
        $phone = Phone::first();
        $this->assertEquals($phone->toArray(), $order->orderDetails->first()->phone->toArray());
        $this->assertEquals('1132289982', $order->orderDetails->first()->phone->number);
    }
}

