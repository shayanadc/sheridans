<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketCodeControlTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_all_valid_ticket_code()
    {
        $this->withoutMiddleware();
        factory(\App\TicketCode::class, 2)->create(['visible' => true]);
        factory(\App\TicketCode::class, 3)->create(['visible' => false]);
        $valids = \App\TicketCode::visible()->get();
        $this->get('/ticketcodes/visible')
            ->seeJson([
                'result' => 'ok',
                'message' => 'all ticket codes',
                'ticketCodes' => $valids->toArray()
            ]);
    }
    /**
     * @test
     */
    public function it_change_state_of_ticketCode_visibility(){
        $this->withoutMiddleware();
        $ticketCode = factory(\App\TicketCode::class)->create(['visible' => true]);
        $this->assertTrue($ticketCode->visible);
        $this->put('/ticketcodes/' . $ticketCode->id  , ['visible' => 0]);
        $tc = \App\TicketCode::find($ticketCode->id);
        $this->assertFalse($tc->visible);
        $this->seeJson([
            'result' => 'ok',
            'message' => 'ticket code' . $tc->id. 'has been updated',
            'ticketCode' => $tc->toArray()
        ]);
    }
}
