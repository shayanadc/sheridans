<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProvinceControlTest extends TestCase
{
   use DatabaseMigrations;
   /**
    * @test
    */
   public function it_returns_all_province(){
      factory(\App\Province::class, 2)->create(['visible' => false]);
      factory(\App\Province::class, 2)->create();
      $provinces = \App\Province::all();
       $this->assertCount(4, $provinces->toArray());
      $this->get('/provinces')
          ->seeJson([
              'result' => 'ok',
              'message' => 'all provinces',
              'provinces' => $provinces->toArray()
          ]);
   }
    /**
     * @test
     */
    public function it_returns_all_visible_province(){
        factory(\App\Province::class, 2)->create(['visible' => false]);
        factory(\App\Province::class, 2)->create();
        $provinces = \App\Province::visible()->with('cities')->get();
        $this->assertCount(2, $provinces->toArray());
        $this->get('/provinces/visible')
            ->seeJson([
                'result' => 'ok',
                'message' => 'all visible provinces',
                'provinces' => $provinces->toArray()
            ]);
    }
   /**
    * @test
    */
   public function it_save_new_province_with_valid_data(){
       $data = $data = [
           'area_code' => str_random(3),
           'name' => str_random(4),
       ];
      $this->post('/provinces/', $data);
      $province = \App\Province::first();
      $this->seeJson([
          'result' => 'ok',
          'message' => 'ticket ' . $province->id . ' has been saved',
          'province' => $province->toArray()
      ]);
   }
   /**
    * @test
    */
   public function it_change_province_information()
   {
      $province = factory(\App\Province::class)->create(['visible' => false]);
      $this->put('/provinces/' . $province->id, ['visible' => true, 'area_code' => '17', 'name' => 'st']);
      $prv = \App\Province::find($province->id);
      $this->assertTrue($prv->visible);
       $this->assertEquals('17', $prv->area_code);
       $this->assertEquals('st', $prv->name);
      $this->seeJson([
          'result' => 'ok',
          'message' => 'province ' . $prv->id . ' has been updated',
          'province' => $prv->toArray()
      ]);
   }
}
