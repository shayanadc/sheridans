<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NekaTopUpEntityControlTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_all_topups(){
        $topups = factory(\App\NekaTopUpEntity::class, 4)->create();
        $tps = \App\NekaTopUpEntity::all();
        $this->get('/topups')->seeJson([
            'result' => 'ok',
            'message' => 'all topup services',
            'topUps' => $tps->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_returns_visibles_topups(){
        $topups = factory(\App\NekaTopUpEntity::class, 2)->create(['visible' => false]);
        $topups = factory(\App\NekaTopUpEntity::class, 2)->create(['visible' => true]);
        $tps = \App\NekaTopUpEntity::visible()->get();
        $this->assertCount(2, $tps);
        $this->get('/topups/visible')->seeJson([
            'result' => 'ok',
            'message' => 'all topup services',
            'topUps' => $tps->toArray(),
        ]);
    }
    /**
     * @test
     */
    public function it_shows_information_of_specific_topup(){
        $topups = factory(\App\NekaTopUpEntity::class)->create();
        $tp = \App\NekaTopUpEntity::find($topups->id);
        $this->get('/topups/' . $tp->id)->seeJson([
            'result' => 'ok',
            'message' => 'topup' . $tp->id .'returned',
            'topUp' => $tp->toArray(),
            'provider' => $tp->provider->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_saves_new_topup_with_valid_data(){
        $data = [
            'title' => str_random(4),
            'description' => str_random(5),
            'price' => random_int(100,600),
            'operator' => str_random(2),
            'special' => true,
            'provider_id' => factory(\App\Provider::class)->create()->id
        ];
        $this->post('/topups', $data);
        $tp = \App\NekaTopUpEntity::first();
        $this->seeJson([
            'result' => 'ok',
            'message' => 'topup ' . $tp->id . ' has been saved',
            'topup' => $tp->toArray()
        ]);
    }
}
