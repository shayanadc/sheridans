<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Mail;


class UserActivationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_error_for_an_invalid_email_address()
    {
        $this->post('/auth/activate', ['email' => 'mail@example'])
            ->seeJson([
                'result' => 'FAIL',
                'message' => 'ایمیل وارد شده معتبر نیست.'
            ]);
    }

    /**
     * @test
     */
    public function it_returns_error_for_an_invalid_mobile_number()
    {
        $this->post('/auth/activate', ['mobile' => '9123257797'])
            ->seeJson([
                'result' => 'FAIL',
                'message' => 'شماره موبایل وارد شده معتبر نیست.'
            ]);
    }

    /**
     * @test
     */
    public function it_sends_an_activation_code_via_email()
    {
        $user = factory(App\User::class)->create(['email' => 'email@example.com']);
        $activationCode = $user->activationCode()->save(factory(App\ActivationCode::class)->make());

        Mail::shouldReceive('queue')
            ->once();

        $this->post('/auth/activate', ['email' => 'email@example.com'])
            ->seeJson(['result' => 'OK']);
    }

    /**
     * @test
     */
    public function it_sends_an_activation_code_via_sms()
    {
        $user = factory(App\User::class)->create(['mobile' => '09123257797']);
        $activationCode = $user->activationCode()->save(factory(App\ActivationCode::class)->make());

        $this->expectsJobs(App\Jobs\SendSMS::class);

        $this->post('/auth/activate', ['mobile' => '09123257797'])
            ->seeJson(['result' => 'OK', 'message' => '']);

    }
}
