<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GateWaysControlTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
   public function it_return_all_gateWays(){
       $gateways = factory(\App\PaymentGateway::class,4)->create();
       $gateways = \App\PaymentGateway::all();
       $this->get('/gateways')->seeJson([
           'result' => 'ok',
           'message' => 'all payment gateways',
           'gateways' => $gateways->toArray()
       ]);
   }
    /**
     * @test
     */
    public function it_saves_new_gateways(){
        $data = [
            'title' => str_random(3),
            'mid' => str_random(4),
            'username' => str_random(4),
            'password' => str_random(4),
            'class_name' => 'App\SamanKish'
        ];

        $this->post('/gateways', $data);
        $gateway = \App\PaymentGateway::where('title',$data['title'])->where('mid', $data['mid'])->where('class_name', $data['class_name'])->first();
            $this->seeJson([
                'result' => 'ok',
                'message' => 'new gateway has been saved',
                'gateway' => $gateway->toArray()
            ]);
    }
    /**
     * @test
     */
    public function it_returns_specific_gateway(){
        $gateway = factory(\App\PaymentGateway::class)->create();
        $this->get('/gateways/' . $gateway->id);
        $gateway = \App\PaymentGateway::find($gateway->id);
        $this->seeJson([
            'result' => 'ok',
            'message' => 'returned'. $gateway->id,
            'gateway' => $gateway->toArray()
        ]);
    }
    /**
     *
     */
    public function it_updates_gateway(){
        $gateway = factory(\App\PaymentGateway::class)->create();
        $this->put('/gatways/' . $gateway->id, ['fallback' => true, 'mid' => '123']);
        $gw = \App\PaymentGateway::find($gateway->id);
        $this->assertEquals('123', $gw->mid);
        $this->assertTrue($gw->fallback);
        $this->assertFalse($gw->enabled);
        $this->seeJson([
            'result' => 'ok',
            'message' => 'gateway ' . $gw->id . ' has been updated',
            'gateway' => $gw->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_updates_enabled_gateway(){
        $gateway = factory(\App\PaymentGateway::class)->create();
        $this->put('/gateways/' . $gateway->id, ['enabled' => true]);
        $gw = \App\PaymentGateway::first();
        $this->assertTrue($gw->enabled);
        $this->assertFalse($gw->fallback);
        $this->seeJson([
            'result' => 'ok',
            'message' => 'gateway ' . $gw->id . ' has been updated',
            'gateway' => $gw->toArray()
        ]);
    }
}
