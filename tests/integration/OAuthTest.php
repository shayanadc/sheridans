<?php

use App\OauthClient;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OAuthTest extends TestCase
{
    protected $client;

    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->client = factory(OauthClient::class)->create();
    }

    /**
     * @test
     */
    public function it_returns_access_and_refresh_token_for_the_valid_email_and_activation_code()
    {
        // create a new user and assign an activation code to it
        $u = factory(User::class)->create();
        $ac = factory(\App\ActivationCode::class)->make();
        $u->activationCode()->save($ac);

        // send request for validating
        $this->post('/access', [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $u->email,
            'password' => $ac->long_code,
        ])
            ->seeJsonStructure([
            "access_token",
            "token_type",
            "expires_in",
            "refresh_token"
        ]);
    }

    /**
     * @test
     */
    public function it_returns_access_and_refresh_token_for_the_valid_mobile_and_activation_code()
    {
        // create a new user and assign an activation code to it
        $u = factory(User::class)->create();
        $ac = factory(\App\ActivationCode::class)->make();
        $u->activationCode()->save($ac);

        // send request for validating
        $this->post('/access', [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $u->mobile,
            'password' => $ac->short_code,
        ])->seeJsonStructure([
            "access_token",
            "token_type",
            "expires_in",
            "refresh_token"
        ]);
    }

    /**
     * @test
     */
    public function it_returns_error_when_invalid_credentials_used_to_get_access_token()
    {
        // create a new user and assign an activation code to it
        $u = factory(User::class)->create();
        $ac = factory(\App\ActivationCode::class)->make();
        $u->activationCode()->save($ac);

        // send request for validating
        $this->post('/access', [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $u->mobile,
            'password' => $ac->long_code . 'A',
        ])
            ->seeJsonStructure(['error', 'error_description'])
        ;
    }

    /**
     * @test
     */
    public function it_returns_valid_access_token_when_using_correct_username_and_password()
    {
        $p = str_random(16);
        
        $u = factory(User::class)->create(['password' => bcrypt($p)]);

        // send request for validating
        $this->post('/access', [
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $u->username,
            'password' => $p,
        ])
            ->seeJsonStructure([
            "access_token",
            "token_type",
            "expires_in",
            "refresh_token"
        ]);
    }
}
