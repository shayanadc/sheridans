<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class UserControlTest extends TestCase
{
  use DatabaseMigrations;
    /**
     * @test
     */
    public function it_saves_user_with_username_password(){
        $data = ['username' => str_random(6), 'password' => str_random(6)];
        $this->post('/users', $data);
        $user = \App\User::first();
        $this->assertEquals($user->username, $data['username']);
        $this->assertTrue(password_verify($data['password'], $user->password));
        $this->seeJson([
        'result' => 'OK',
            'message' => '',
            'user' => $user->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_dosent_save_user_with_duplicate_username(){
        $user = factory(\App\User::class)->create(['username' => '123456']);
        $data = ['username' => $user->username, 'password' => str_random(6)];
        $this->post('/users', $data);
        $this->seeJson([
            'result' => 'FAIL',
            'message' => 'نام کاربری تکراری است.'
        ]);
    }
    /**
     * @test
     */
    public function it_return_all_users(){
        factory(\App\User::class,4)->create();
        $this->get('/users');
        $users = \App\User::with('mobiles', 'landLines')->get();
        $this->seeJson([
           'result' => 'OK',
            'message' => '',
            'users' => $users->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_returns_specific_user_with_phones(){
        $user = factory(\App\User::class)->create();
        $mobile = factory(App\Phone::class)->create(['mobile' => true]);
        $landLine = factory(App\Phone::class)->create(['mobile' => false]);
        $user->setDefaultMobile($mobile);
        $user->setDefaultLandLine($landLine);
        $this->get('/users/'. $user->id);
        $user = \App\User::find($user->id);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'user' => $user->toArray(),
            'landLines' => $user->landLines->toArray(),
            'mobiles' => $user->mobiles->toArray(),
            'defaultLandLine' => $user->defaultLandLine->toArray(),
            'defaultMobile' => $user->defaultMobile->toArray()
        ]);
    }
    /**
     * @test
     */
    public function it_update_user_field_by_request(){
        $user = factory(\App\User::class)->create(['mobile' => '09112718982']);
        $this->put('/users/'. $user->id, ['mobile' => '09397730108']);
        $user = \App\User::find($user->id);
        $this->assertEquals('09397730108', $user->mobile);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'user' => $user->toArray(),
            'landLines' => $user->landLines->toArray(),
            'mobiles' => $user->mobiles->toArray(),
            'defaultLandLine' => null,
            'defaultMobile' => null
            ]);
    }
}