<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthUserControllerTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_all_phone_of_user(){
        $this->withoutMiddleware();
        $user = factory(User::class)->create();
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);
        $user = \App\User::first();
        $phone1 = factory(\App\Phone::class)->create(['mobile' => true]);
        $phone2 = factory(\App\Phone::class)->create(['mobile' => false]);
        $user->phones()->sync([
            $phone1->id,
            $phone2->id
        ]);
        $this->get('auth/user');
        $landLines = $user->landLines;
        $landLine = $user->defaultLandLines;
        $mobiles = $user->mobiles;
        $mobile = $user->defaultMobile;
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'mobiles' => $mobiles->toArray(),
            'landLines' => $landLines->toArray(),
            'defaultMobile' => $mobile,
            'defaultLandLine' => $landLine
        ]);
    }
    /**
     * @test
     */
    public function it_update_user_field_by_request(){
        $this->withoutMiddleware();
        $user = factory(\App\User::class)->create(['mobile' => '09112718982']);
        Authorizer::shouldReceive('getResourceOwnerId')
            ->andReturn($user->id);
        $this->put('auth/user/' , ['mobile' => '09397730108']);
        $user = \App\User::find($user->id);
        $this->assertEquals('09397730108', $user->mobile);
        $this->seeJson([
            'result' => 'OK',
            'message' => '',
            'user' => $user->toArray(),
        ]);
    }
}
