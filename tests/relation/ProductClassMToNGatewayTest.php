<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductClassMToNGatewayTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->pg1 = factory(\App\PaymentGateway::class)->create(['fallback' => false, 'enabled' => true]);
        $this->pg2 = factory(\App\PaymentGateway::class)->create(['fallback' => false, 'enabled' => true]);
        $this->pg3 = factory(\App\PaymentGateway::class)->create(['fallback' => true, 'enabled' => true]);

        $this->pt1 = factory(App\ProductClass::class)->create();
        $this->pt2 = factory(App\ProductClass::class)->create();
        $this->pt3 = factory(App\ProductClass::class)->create();

//        $this->pg1->productTypes()->sync([
//            $this->pt1->id,
//            $this->pt2->id
//        ]);
        $this->pt1->paymentGateWays()->sync([
            $this->pg1->id,
            $this->pg2->id
        ]);
    }
    /**
     * @test
     */
    public  function it_returns_all_gateways_of_specific_product_type(){
        $productType1GateWays =$this->pt1->getPaymentGateways();
        $this->assertCount(2, $productType1GateWays);
        $pgs = App\ProductClass::find($this->pt1->id)->getPaymentGateways();
        $this->assertEquals($productType1GateWays->toArray(), $pgs->toArray());
    }

    /**
     * @test
     */

    public function it_returns_fallback_if_dont_set_gateway(){
        $pt3 = \App\ProductClass::find($this->pt3->id);
        $fg = \App\PaymentGateway::where('id', $this->pg3->id)->get();
        $this->assertEquals($fg->toArray(),$pt3->getPaymentGateways()->toArray());
    }
}