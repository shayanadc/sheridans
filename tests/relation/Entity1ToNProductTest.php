<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Entity1ToNProductTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_entity_of_type_topup()
    {
        $topup = factory(App\NekaTopUpEntity::class)->create();

        $p = factory(App\Product::class)->make();
        $topup->products()->save($p);

        $p = App\Product::first();

        $this->assertEquals($topup->id, $p->entity->id);
        $this->assertInstanceOf(App\NekaTopUpEntity::class, $p->entity);
    }

    /**
     * @test
     */
    public function it_returns_entity_of_type_partaak()
    {
        $partaak = factory(App\PartaakProduct::class)->create();

        $p = factory(App\Product::class)->make();
        $partaak->products()->save($p);

        $p = App\Product::first();

        $this->assertEquals($partaak->id, $p->entity->id);
        $this->assertInstanceOf(App\PartaakProduct::class, $p->entity);
    }

    /**
     * @test
     */
    public function it_returns_product_for_a_topup_entity()
    {
        $topup = factory(App\NekaTopUpEntity::class)->create();

        $p = factory(App\Product::class)->make();
        $topup->products()->save($p);

        $p = App\Product::first();

        $this->assertEquals($p->id, $topup->products->first()->id);
        $this->assertContainsOnly(App\Product::class, $topup->products);
    }

    /**
     * @test
     */
    public function it_returns_product_for_a_partaak_entity()
    {
        $partaak = factory(App\PartaakProduct::class)->create();

        $p = factory(App\Product::class)->make();
        $partaak->products()->save($p);

        $p = App\Product::first();

        $this->assertEquals($p->id, $partaak->products->first()->id);
        $this->assertContainsOnly(App\Product::class, $partaak->products);
    }

    /**
     * @test
     */
    public function it_checks_product_accessor(){
        factory(\App\Product::class)->create();
        $product = \App\Product::first();
        $this->assertNotEmpty($product->title);
        $this->assertNull($product->description);
        $this->assertNotEmpty($product->price);
        $this->assertTrue($product->visible);
    }
}
