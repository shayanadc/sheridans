<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderDetailNTo1ProductsTest extends TestCase
{
    use DatabaseMigrations;

    protected $product1;
    protected $product2;
    protected $orderDetail1;
    protected $orderDetail2;

    public function setUp()
    {
        parent::setUp();

        $product1 = factory(App\Product::class)->create();
        $product2 = factory(App\Product::class)->create();
        $this->product1 = App\Product::find($product1->id);
        $this->product2 = App\Product::find($product2->id);
        $orderDetail1 = factory(App\OrderDetail::class)->create(['product_id' => $product1->id]);
        $orderDetail2 = factory(App\OrderDetail::class)->create(['product_id' => $product1->id]);
        $this->orderDetail1 = App\OrderDetail::find($orderDetail1->id);
        $this->orderDetail2 = App\OrderDetail::find($orderDetail2->id);
    }
    /**
     * @test
     */
    public function it_returns_product_of_an_order_detail(){
        $order1Product = $this->orderDetail1->product;
        $this->assertInstanceOf(\App\Product::class, $order1Product);
        $this->assertEquals($this->product1, $order1Product);
    }
    /**
     * @test
     */
    public function it_returns_all_orders_of_a_product()
    {
        $product1Orders = $this->product1->orderDetails;
        $this->assertCount(2, $product1Orders);
        $this->assertContains($this->orderDetail1->toArray(), $product1Orders->toArray());
    }

    /**
     * @test
     */
    public function it_returns_null_if_has_not_any_order_for_product(){
        $product2Orders = $this->product2->orderDetails;
        $this->assertCount(0, $product2Orders);
    }
}
