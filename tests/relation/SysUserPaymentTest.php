<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SysUserPaymentTest extends TestCase
{
    use DatabaseMigrations;
    protected $user1;
    protected $sysUser1;
    protected $sysUser2;
    protected $payment1;
    protected $payment2;
    protected $payment3;

    public function setUp()
    {
        parent::setUp();

        $user1 = factory(App\User::class)->create();
        $sysUser1 = factory(App\SystemUser::class)->create();
        $sysUser2 = factory(App\SystemUser::class)->create();
        $this->user1 = App\User::find($user1->id);
        $this->sysUser1 = App\SystemUser::find($sysUser1->id);
        $this->sysUser2 = App\SystemUser::find($sysUser2->id);

        $payment1 = factory(App\Payment::class)
            ->create(['registrant_id' => $sysUser1->id, 'registrant_type' => get_class($sysUser1)]);
        $payment2 = factory(App\Payment::class)->create(['registrant_id' => $sysUser1->id, 'registrant_type' => get_class($sysUser1)]);
        $payment3 = factory(App\Payment::class)->create(['registrant_id' => $user1->id, 'registrant_type' => get_class($user1)]);

        $this->payment1 = App\Payment::find($payment1->id);
        $this->payment2 = App\Payment::find($payment2->id);
        $this->payment3 = App\Payment::find($payment3->id);
    }

    /**
     * @test
     */
    public function it_returns_all_payment_of_a_systemUser()
    {
        $user1Payments = $this->sysUser1->payments;
        $this->assertCount(2, $user1Payments);
        $this->assertContains($this->payment1->toArray(), $user1Payments->toArray());
    }

    /**
     * @test
     */
    public function it_returns_all_systemUser_of_a_payment(){
        $payment1User = $this->payment1->registrant;
        $this->assertInstanceOf(\App\SystemUser::class, $payment1User);
        $this->assertEquals($payment1User->id, $this->sysUser1->id);
    }
    /**
     * @test
     */
    public function it_returns_all_user_of_a_payment(){
        $payment3User = $this->payment3->registrant;
        $this->assertInstanceOf(\App\User::class, $payment3User);
        $this->assertEquals($payment3User->id, $this->user1->id);
    }
}
