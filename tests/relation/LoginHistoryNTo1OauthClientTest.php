<?php

use App\OauthClient;
use App\LoginHistory;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginHistoryNTo1OauthClientTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_oauth_client_of_a_login_history()
    {
        $oc = factory(OauthClient::class)->create();
        $lh = factory(LoginHistory::class)->create(['oauth_client_id' => $oc->id]);

        $this->assertEquals($oc->id, $lh->oauthClient->id);
    }

    /**
     * @test
     */
    public function it_returns_login_history_of_a_client()
    {
        $oc = factory(OauthClient::class)->create();
        factory(LoginHistory::class)->create(['oauth_client_id' => $oc->id]);
        factory(LoginHistory::class)->create();
        factory(LoginHistory::class)->create(['oauth_client_id' => $oc->id]);

        $this->assertCount(2, $oc->loginHistories);
        $this->assertContainsOnly(LoginHistory::class,$oc->loginHistories);
    }
}
