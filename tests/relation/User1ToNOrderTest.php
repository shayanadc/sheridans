<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Order;
use App\User;

class User1ToNOrderTest extends TestCase
{
    use DatabaseMigrations;
    protected $user1;
    protected $user2;
    protected $issuer1;
    protected $order1;
    protected $order2;
    protected $order3;

    public function setUp()
    {
        parent::setUp();

        $user1 = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();
        $issuer1 = factory(App\User::class)->create();
        $this->user1 = App\User::find($user1->id);
        $this->user2 = App\User::find($user2->id);
        $this->issuer1 = App\User::find($issuer1->id);
        $order1 = factory(App\Order::class)->create(['user_id' => $user1->id]);
        $order2 = factory(App\Order::class)->create(['user_id' => $user2->id]);
        $order3 = factory(App\Order::class)->create(['issuer_id' => $issuer1->id]);
        $this->order1 = App\Order::where('user_id', $user1->id)->first();
        $this->order2 = App\Order::where('user_id', $user2->id)->first();
        $this->order3 = App\Order::find($order3->id);
    }
    /**
     * @test
     */
    public function it_returns_all_orders_of_a_user()
    {
        $user1Orders = $this->user1->orders;
        $this->assertCount(1, $user1Orders);
        $this->assertEquals($this->order1->toArray(), $user1Orders->first()->toArray());
    }
    /**
     * @test
     */
    public function it_returns_user_of_a_specific_order()
    {
        $order1User = $this->order1->user;
        $this->assertInstanceOf(User::class, $order1User);
        $this->assertEquals($this->order1->user->id, $this->user1->id);
    }
    /**
     * @test
     */
    public function it_returns_issuer_of_an_order(){
        $order3Issuer = $this->order3->issuer;
        $this->assertInstanceOf(\App\User::class, $order3Issuer);
        $this->assertEquals($order3Issuer->id, $this->issuer1->id);
    }
    /**
     * @test
     */
    public function it_returns_all_orders_that_issued_by_a_specific_user(){
        $user1Orders = $this->issuer1->issuedOrders;
        $this->assertCount(1, $user1Orders);
        $this->assertContains($this->order3->toArray(), $user1Orders->toArray());
    }

}
