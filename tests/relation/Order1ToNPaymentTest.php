<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Order1ToNPaymentTest extends TestCase
{
    use DatabaseMigrations;

    protected $order1;
    protected $order2;
    protected $paymet1;
    protected $paymet2;

    public function setUp()
    {
        parent::setUp();
        $order1 = factory(App\Order::class)->create();
        $order2 = factory(App\Order::class)->create();
        $this->order1 = App\Order::find($order1->id);
        $this->order2 = App\Order::find($order2->id);

        $payment1 = factory(App\Payment::class)->create(['order_id' => $order1->id]);
        $payment2 = factory(App\Payment::class)->create(['order_id' => $order1->id, 'confirmed' => true]);
        $this->paymet1 = App\Payment::find($payment1->id);
        $this->paymet2 = App\Payment::find($payment2->id);
    }
    /**
     * @test
     */
    public function it_return_all_payments_for_a_specific_order(){
        $order1Payment = $this->order1->payments;
        $this->assertCount(2, $order1Payment);
        $payments = App\Payment::where('order_id', $this->order1->id)->get();
        $this->assertEquals($order1Payment->toArray(), $payments->toArray());
    }

    /**
     * @test
     */
    public function it_return_null_for_order_has_not_any_payment(){
        $this->assertEmpty($this->order2->payments);
    }
    /**
     * @test
     */
    public function it_return_order_of_a_payment(){
        $this->assertEquals($this->paymet1->order->id, $this->order1->id);
    }
    /**
     * @test
     */
    public function it_return_all_confirmed_payments_for_a_specific_order(){
        $order1Payment = $this->order1->confirmedPayments;
        $this->assertCount(1, $order1Payment);
        $payments = App\Payment::where('order_id', $this->order1->id)->where('confirmed', true)->get();
        $this->assertEquals($order1Payment->toArray(), $payments->toArray());
    }
}
