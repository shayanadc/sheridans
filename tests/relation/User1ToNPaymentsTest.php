<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class User1ToNPaymentsTest extends TestCase
{
    use DatabaseMigrations;
    protected $user1;
    protected $user2;
    protected $order1;
    protected $order2;
    protected $payment1;
    protected $payment2;

    public function setUp()
    {
        parent::setUp();

        $user1 = factory(App\User::class)->create();
//        $user2 = factory(App\User::class)->create();
        $this->user1 = App\User::find($user1->id);
//        $this->user2 = App\User::find($user2->id);
        $order1 = factory(App\Order::class)->create(['user_id' => $user1->id]);
//        $order2 = factory(App\Order::class)->create(['user_id' => $user2->id]);
        $this->order1 = App\Order::where('user_id', $user1->id)->first();
//        $this->order2 = App\Order::where('user_id', $user2->id)->first();
        $payment1 = factory(\App\Payment::class)->create(['order_id' => $this->order1->id]);
//        $payment2 = factory(\App\Payment::class)->create(['order_id' => $this->order1]);
        $this->payment1 = \App\Payment::find($payment1->id);
//        $this->payment2 = \App\Payment::find($payment2->id);
    }
    /**
     * @test
     */
    public function it_returns_all_payments_of_a_user(){
//dd($this->user1->toArray(),$this->order1->toArray(),$this->payment1->toArray());
        $user1Payments = $this->user1->payments;
        $this->assertInstanceOf(\App\Payment::class, $user1Payments->first());
        $this->assertCount(1, $user1Payments);
        $payments = \App\Payment::all();
//        dd($user1Payments->toArray(),$payments->toArray());
        $this->assertArraySubset($payments->toArray(), $user1Payments->toArray());
    }
}
