<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Product1ToNGatewayTest extends TestCase
{
   use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_gateways_of_a_product(){

        $this->pg1 = factory(\App\PaymentGateway::class)->create();
        $this->pg2 = factory(\App\PaymentGateway::class)->create();
        $this->pg3 = factory(\App\PaymentGateway::class)->create();

        $this->pt1 = factory(App\ProductClass::class)->create(['name' => 'App\NekaTopUpEntity']);
        $this->product = factory(App\NekaTopUpEntity::class)->create();
        $this->pt1->paymentGateways()->sync([
            $this->pg1->id,
            $this->pg2->id
        ]);
        $product = \App\NekaTopUpEntity::first();
        $pt1 = \App\ProductClass::first();
        $this->assertEquals($product->pgs()->toArray(), $pt1->getPaymentGateways()->toArray());
    }
}
