<?php

use App\ProductEntity;
use App\Provider;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductNTo1ProviderTest extends TestCase
{
    use DatabaseMigrations;
    public function setUp()
    {
        parent::setUp();
        factory(\App\ProductClass::class)->create(['name' => 'App\NekaTopUpEntity']);
    }
    /**
     * @test
     */
    public function it_returns_null_for_products_of_a_provider()
    {
        $provider = factory(Provider::class)->create();
        factory(\App\NekaTopUpEntity::class, 5)->create();

        $this->assertEmpty($provider->products());
    }

    /**
     * @test
     */
    public function it_returns_two_products_for_a_provider()
    {
        $provider = factory(Provider::class)->create();
        factory(\App\NekaTopUpEntity::class)->create(['provider_id' => $provider->id]);
        factory(\App\NekaTopUpEntity::class,5)->create();
        factory(\App\NekaTopUpEntity::class)->create(['provider_id' => $provider->id]);
        $this->assertCount(2, $provider->products());
    }

    /**
     * @test
     */
    public function it_returns_provider_for_a_product()
    {
        $provider = factory(Provider::class)->create();
        $product = factory(\App\NekaTopUpEntity::class)->create(['provider_id' => $provider->id]);

        $this->assertEquals($provider->id, $product->provider->id);
    }
}
