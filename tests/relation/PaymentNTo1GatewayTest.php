<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentNTo1GatewayTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     */
    public function it_returns_many_payment_for_a_gateway(){
        $gw1 = factory(App\PaymentGateway::class)->create();
        $gw2 = factory(App\PaymentGateway::class)->create();
        $payment1 = factory(App\Payment::class)->create(['gw' => $gw1->id]);
        $payment2 = factory(App\Payment::class)->create(['gw' => $gw1->id]);
        $payment3 = factory(App\Payment::class)->create(['gw' => $gw2->id]);
        $payment = \App\Payment::where('gw', $gw1->id)->get();
        $this->assertCount(2, $gw1->payments);
        $this->assertEquals($payment->toArray(), $gw1->payments->toArray());
    }
    /**
     * @test
     */
    public function it_return_gw_for_specific_payment(){
        $gw1 = factory(App\PaymentGateway::class)->create();
        $gw2 = factory(App\PaymentGateway::class)->create();
        $payment = factory(\App\Payment::class)->create(['gw' => $gw1->id]);
        $this->assertEquals($gw1->id, $payment->gateway->id);
    }
}
