<?php

use App\User;
use App\LoginHistory;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class User1ToNLoginHistory extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function it_returns_last_logins_for_a_user()
    {
        $user = factory(User::class)->create();
        factory(LoginHistory::class)->create(['user_id' => $user->id]);
        factory(LoginHistory::class,5)->create();
        factory(LoginHistory::class)->create(['user_id' => $user->id]);

        $this->assertCount(2, $user->lastLogins());
    }

    /**
     * @test
     */
    public function it_returns_the_user_of_a_login_history()
    {
        $user = factory(User::class)->create();
        $lh = factory(LoginHistory::class)->create(['user_id' => $user->id]);

        $this->assertInstanceOf(User, $lh->user);
        $this->assertEquals($user->id, $lh->user->id);
    }
}
