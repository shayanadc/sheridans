<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SysUser1ToNOrdeDetilsTest extends TestCase
{
    use DatabaseMigrations;
    protected $user1;
    protected $sysUser1;
    protected $sysUser2;
    protected $orderDetail1;
    protected $orderDetail2;

    public function setUp()
    {
        parent::setUp();

        $user1 = factory(App\User::class)->create();
        $sysUser1 = factory(App\SystemUser::class)->create();
        $sysUser2 = factory(App\SystemUser::class)->create();
        $this->user1 = App\User::find($user1->id);
        $this->sysUser1 = App\SystemUser::find($sysUser1->id);
        $this->sysUser2 = App\SystemUser::find($sysUser2->id);

        $detail1 = factory(App\OrderDetail::class)->create(['assignee_id' => $sysUser1->id, 'assignee_type' => get_class($sysUser1)]);
        $detail2 = factory(App\OrderDetail::class)->create(['assignee_id' => $sysUser1->id, 'assignee_type' => get_class($sysUser1)]);
        $detail3 = factory(App\OrderDetail::class)->create(['assignee_id' => $user1->id, 'assignee_type' => get_class($user1)]);

        $this->orderDetail1 = App\OrderDetail::find($detail1->id);
        $this->orderDetail2 = App\OrderDetail::find($detail2->id);
        $this->orderDetail3 = App\OrderDetail::find($detail3->id);
    }
    /**
     * @test
     */
    public function it_returns_all_order_details_of_a_systemUser()
    {
        $user1Orders = $this->sysUser1->orderDetails;
        $this->assertCount(2, $user1Orders);
        $this->assertContains($this->orderDetail1->toArray(), $user1Orders->toArray());
    }
    /**
     * @test
     */
    public function it_returns_all_systemUser_of_a_order_detail()
    {
        $order1User = $this->orderDetail1->assignee;
        $this->assertInstanceOf(\App\SystemUser::class, $order1User);
        $this->assertEquals($order1User->id, $this->sysUser1->id);
    }
    /**
     * @test
     */
    public function it_returns_all_user_of_a_order_detail()
    {
        $order1User = $this->orderDetail3->assignee;
        $this->assertInstanceOf(\App\User::class, $order1User);
        $this->assertEquals($order1User->id, $this->user1->id);
    }
}
