<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderDetailNTo1ProviderTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * @test
     */
    public function it_returns_null_for_provider_with_no_order_details()
    {
        $order = factory(App\Order::class)->create();
        $provider = factory(App\Provider::class)->create();
        $this->assertEmpty($provider->orderDetails);
    }

    /**
     * @test
     */
    public function it_returns_many_order_details_for_a_provider()
    {
        $provider = factory(App\Provider::class)->create();
        factory(App\OrderDetail::class)->create(['provider_id' => $provider->id]);
        factory(App\OrderDetail::class,4);
        factory(App\OrderDetail::class)->create(['provider_id' => $provider->id]);

        $this->assertCount(2, $provider->orderDetails);
    }

    /**
     * @test
     */
    public function it_returns_provider_of_an_order_detail()
    {
        $provider = factory(App\Provider::class)->create();
        $orderDetail = factory(App\OrderDetail::class)->create(['provider_id' => $provider->id]);

        $this->assertInstanceOf(App\Provider::class, $orderDetail->provider);
        $this->assertEquals($provider->id, $orderDetail->provider->id);
    }
}
