<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductNTo1TypeTest extends TestCase
{
    use DatabaseMigrations;
    protected $product1;
    protected $product2;
    protected $pType1;
    protected $pType2;

    public function setUp()
    {
        parent::setUp();

        $pType2 = factory(\App\ProductType::class)->create(['title' => 'topup']);
        $pType1 = factory(\App\ProductType::class)->create(['title' => 'adsl']);
        $this->pType1 = \App\ProductType::find($pType1->id);
        $this->pType2 = \App\ProductType::find($pType2->id);
        $product1 = factory(\App\Product::class)->create(['product_type_id' => $this->pType1->id]);
        $this->product1 = \App\Product::find($product1->id);
    }
    /**
     * @test
     */
    public function it_returns_all_product_of_specific_type(){
        $this->assertCount(0, $this->pType2->products);
        $this->assertCount(1, $this->pType1->products);
        $this->assertContains($this->product1->toArray(), $this->pType1->products->toArray());
    }
    /**
     * @test
     */
    public function it_returns_type_of_product(){
        $this->assertEquals($this->pType1->toArray(), $this->product1->productType->toArray());
    }
}
