<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Phone1ToNOrderDetailTest extends TestCase
{
    use DatabaseMigrations;

    protected $phone1;
    protected $phone2;
    protected $details1;
    protected $details2;

    public function setUp()
    {
        parent::setUp();

        $this->phone1 = factory(App\Phone::class)->create(['mobile' => false]);
        $this->phone1 = App\Phone::find($this->phone1->id);
        $this->phone2 = factory(App\Phone::class)->create(['mobile' => true]);
        $this->phone2 = App\Phone::find($this->phone2->id);
        factory(App\OrderDetail::class,4)->create(['phone_id' => $this->phone1->id]);
        $this->details1 = App\OrderDetail::where('phone_id', $this->phone1->id)->get();
        factory(App\OrderDetail::class,3)->create(['phone_id' => $this->phone2->id]);
        $this->details2 = App\OrderDetail::where('phone_id', $this->phone2->id)->get();
    }

    /**
     * @test
     */
    public function it_returns_all_order_details_of_a_phone()
    {
        $ods1 = $this->phone1->orderDetails;
        $this->assertCount(4, $ods1);
        foreach ($this->details1 as $detail) {
            $this->assertContains($detail->toArray(), $ods1->toArray());
        }

        $ods2 = $this->phone2->orderDetails;
        $this->assertCount(3, $ods2);
        foreach ($this->details2 as $detail) {
            $this->assertContains($detail->toArray(), $ods2->toArray());
        }
    }

    /**
     * @test
     */
    public function it_returns_phone_of_an_order_detail()
    {
        foreach ($this->details1 as $detail) {
            $this->assertEquals($this->phone1, $detail->phone);
        }

        foreach ($this->details2 as $detail) {
            $this->assertEquals($this->phone2, $detail->phone);
        }

    }
}
