<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProviderNToMCityTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->city1 = factory(\App\City::class)->create();
        $this->city2 = factory(\App\City::class)->create();
        $this->city3 = factory(\App\City::class)->create();
        $this->city = factory(\App\City::class)->create();

        $this->provider1 = factory(App\Provider::class)->create();
        $this->provider2 = factory(App\Provider::class)->create();
        $this->provider3 = factory(App\Provider::class)->create();
        $this->provider4 = factory(App\Provider::class)->create();

        $this->city1->providers()->sync([
            $this->provider1->id,
            $this->provider2->id,
            $this->provider3->id
        ]);
        $this->city2->providers()->sync([
            $this->provider4->id,
            $this->provider3->id
        ]);
    }
    /**
     * @test
     */
    public function it_return_cities_of_a_specific_provider(){
        $provider3Cities = $this->provider3->cities;
        $this->assertCount(2, $provider3Cities);
        $cities = App\Provider::find($this->provider3->id)->cities;
        $this->assertEquals($provider3Cities->toArray(), $cities->toArray());
    }
    /**
     * @test
     */
    public function it_returns_all_the_providers_of_a_city()
    {
        $this->assertCount(3, $this->city1->providers);
        $this->assertCount(2, $this->city2->providers);
    }
    /**
     * @test
     */
    public function it_returns_null_for_a_city_without_a_provider()
    {
        $this->assertEmpty($this->city->providers);
    }

}
