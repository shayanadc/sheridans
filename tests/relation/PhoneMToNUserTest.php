<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PhoneMToNUserTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->user1 = factory(\App\User::class)->create();
        $this->user2 = factory(\App\User::class)->create();
        $this->user3 = factory(\App\User::class)->create();

        $this->phone1 = factory(App\Phone::class)->create(['mobile' => false]);
        $this->phone2 = factory(App\Phone::class)->create(['mobile' => false]);
        $this->phone3 = factory(App\Phone::class)->create(['mobile' => false]);
        $this->phone4 = factory(App\Phone::class)->create(['mobile' => false]);

        $this->mobile1 = factory(App\Phone::class)->create(['mobile' => true]);
        $this->mobile2 = factory(App\Phone::class)->create(['mobile' => true]);
        $this->mobile3 = factory(App\Phone::class)->create(['mobile' => true]);

        $this->user1->phones()->sync([
            $this->phone1->id => ['default' => true],
            $this->phone2->id,
            $this->mobile1->id,
            $this->mobile2->id,
        ]);

        $this->user2->phones()->sync([
            $this->phone3->id,
            $this->phone2->id,
            $this->mobile1->id,
            $this->mobile2->id => ['default' => true],
            $this->mobile3->id,
        ]);

        $this->user3->phones()->sync([
            $this->phone1->id => ['default' => true]
        ]);
    }

    /**
     * @test
     */
    public function it_returns_all_the_phones_of_a_user()
    {
        $this->assertCount(4, $this->user1->phones);
        $this->assertCount(5, $this->user2->phones);
    }

    /**
     * @test
     */
    public function it_returns_all_the_mobile_phones_of_a_user()
    {
        $this->assertCount(2, $this->user1->mobiles);
        $this->assertCount(3, $this->user2->mobiles);
    }

    /**
     * @test
     */
    public function it_returns_all_the_land_line_phones_of_a_user()
    {
        $this->assertCount(2, $this->user1->landLines);
        $this->assertCount(2, $this->user2->landLines);
    }

    /**
     * @test
     */
    public function it_returns_the_default_mobile_phone_of_a_user()
    {

        $this->assertEquals($this->mobile2->id, $this->user2->defaultMobile->id);
    }

    /**
     * @test
     */
    public function it_returns_null_for_a_user_without_a_default_mobile_phone()
    {
        $this->assertNull($this->user1->defaultMobile);
    }

    /**
     * @test
     */
    public function it_returns_the_default_land_line_phone_of_a_user()
    {
        $this->assertEquals($this->phone1->id, $this->user1->defaultLandLine->id);
    }

    /**
     * @test
     */
    public function it_returns_null_for_a_user_without_a_default_land_line_phone()
    {
        $this->assertNull($this->user2->landLine);
    }

    /**
     * @test
     */
    public function it_returns_all_the_users_of_a_phone()
    {
        $this->assertCount(2, $this->phone2->users);
        $this->assertCount(1, $this->phone3->users);
        $this->assertCount(0, $this->phone4->users);
    }

    /**
     * @test
     */
    public function it_returns_all_the_users_with_the_phone_set_as_default()
    {
        $this->assertArraySubset([$this->user1->toArray(), $this->user3->toArray()], $this->phone1->defaultUsers->toArray());
    }

    /**
     * @test
     */
    public function it_sets_a_users_default_mobile_phone()
    {
        $this->assertNull($this->user1->defaultMobile);
        $this->user1->setDefaultMobile($this->mobile3);
        $this->assertEquals($this->mobile3->id, $this->user1->defaultMobile->id);
    }

    /**
     * @test
     */
    public function it_sets_a_users_default_land_line_phone() {
        $this->assertNull($this->user2->defaultLandLine);
        $this->user2->setDefaultLandLine($this->phone3);
        $this->assertEquals($this->phone3->id, $this->user2->defaultLandLine->id);
    }

    /**
     * @test
     */
    public function it_updates_a_users_default_mobile_phone() {
        $this->assertEquals($this->mobile2->id,$this->user2->defaultMobile->id);
        $this->user2->setDefaultMobile($this->mobile3);
        $this->assertEquals($this->mobile3->id, $this->user2->defaultMobile->id);
    }

    /**
     * @test
     */
    public function it_updates_a_users_default_land_line_phone() {
        $this->assertEquals($this->phone1->id, $this->user1->defaultLandLine->id);
        $this->user1->setDefaultLandLine($this->phone3);
        $this->assertEquals($this->phone3->id, $this->user1->defaultLandLine->id);
    }
}
