<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Order1ToNOrderDetailsTest extends TestCase
{
    use DatabaseMigrations;
    protected $order1;
    protected $order2;
    protected $orderDetail1;
    protected $orderDetail2;

    public function setUp()
    {
        parent::setUp();
        $order1 = factory(App\Order::class)->create();
        $order2 = factory(App\Order::class)->create();
        $this->order1 = \App\Order::find($order1->id);
        $this->order2 = \App\Order::find($order2->id);
        $orderDetail1 = factory(App\OrderDetail::class)->create(['order_id' => $order1->id]);
        $orderDetail2 = factory(App\OrderDetail::class)->create(['order_id' => $order1->id]);
        $this->orderDetail1 = App\OrderDetail::find($orderDetail1->id);
        $this->orderDetail2 = App\OrderDetail::find($orderDetail2->id);
    }
    /**
     * @test
     */
    public function it_return_all_orderDetail_of_specific_order(){
        $order1Detail = $this->order1->orderDetails;
        $this->assertCount(2, $order1Detail);
        $order1Details = \App\OrderDetail::where('order_id', $this->order1->id)->get();
        $this->assertEquals($order1Details->toArray(), $order1Detail->toArray());
    }
    /**
     * @test
     */
    public function it_returns_order_of_a_specific_detail()
    {
        $order1 = $this->orderDetail1->order;
        $this->assertInstanceOf(\App\Order::class, $order1);
        $this->assertEquals($order1->id, $this->order1->id);
    }
}
