<?php

use App\PaymentGateway;
use App\NekaTopUpEntity;
use App\User;
use App\ActivationCode;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'mobile' => $faker->phoneNumber,
        'username' => strval(random_int(100000,999999)),
        'password' => bcrypt(strval(random_int(100000,999999))),
        'remember_token' => str_random(10),
        'role' => User::CUSTOMER_ROLE,
    ];
});
$factory->define(ActivationCode::class, function (Faker\Generator $faker) {
    $u = factory(User::class)->create();
    return [
        'user_id' => $u->id,
        'long_code' => str_random(40),
        'short_code' => strval(random_int(100000, 999999)),
    ];
});
$factory->define(NekaTopUpEntity::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(3),
        'provider_id' => factory(\App\Provider::class)->create()->id,
        'description' => $faker->sentence(10),
        'price' => $faker->randomNumber(2),
        'visible' => $faker->boolean(10),
        'operator' => $faker->randomElement(['mtn','mci','rgt']),
        'special' => $faker->boolean(10),
    ];
});
$factory->define(App\PartaakProduct::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(3),
        'provider_id' => factory(\App\Provider::class)->create()->id,
        'description' => $faker->sentence(10),
        'price' => $faker->randomNumber(2),
        'visible' => $faker->boolean(10),
        'rate' => 1,
        'vol' => 2,
        'period' => 2,
        'product_id' => 2
    ];
});
$factory->define(App\OrderDetail::class, function (Faker\Generator $faker) {
    $pg = factory(PaymentGateway::class)->create();
    return [
        'order_id' => factory(\App\Order::class)->create()->id,
        'provider_id' => factory(\App\Provider::class)->create()->id,
        'product_id' => factory(\App\Product::class)->create()->id,
        'phone_id' => factory(\App\Phone::class)->create()->id,
        'qty' => $faker->randomNumber(1),
        'unit_price' => $faker->randomFloat(2, 1, 8),
    ];
});
$factory->define(App\Order::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\User::class)->create()->id,
        'total_amount' => $faker->randomFloat(2, 2, 50),
        'total_qty' => $faker->randomNumber(1),
    ];
});
$factory->define(App\SystemUser::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word
    ];
});
$factory->define(App\PaymentGateway::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'enabled' => $faker->boolean(10),
        'mid' => $faker->randomNumber(6),
        'username' =>$faker->sentence(10),
        'password' => $faker->sentence(10),
        'class_name' => $faker->randomElement(['App\IranKishPG','App\IranKishPG','App\SamanKishPG']),
    ];
});
$factory->define(App\Payment::class, function (Faker\Generator $faker) {
    return [
        'order_id' => factory(\App\Order::class)->create()->id,
        'amount' => $faker->randomFloat(2, 2, 50),
        'gw' => factory(PaymentGateway::class)->create()->id,
        'method' => $faker->randomElement(['deposit','cash']),
    ];
});
$factory->define(App\TicketCode::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->word,
        'visible' => $faker->boolean(10),
    ];
});
$factory->define(App\Ticket::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(\App\User::class)->create()->id,
        'err_code' => factory(\App\TicketCode::class)->create()->id,
        'closed' => $faker->boolean(10)
    ];
});
$factory->define(App\Phone::class, function (Faker\Generator $faker) {
    return [
        'number' => $faker->phoneNumber,
        'mobile' => $faker->boolean(10)
    ];
});

$factory->define(App\OauthClient::class, function (Faker\Generator $faker) {
    return [
        'id' => str_random(16),
        'secret' => str_random(40),
        'name' => $faker->word
    ];
});

$factory->define(App\Province::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'area_code' => $faker->unique()->randomNumber(3),
    ];
});

$factory->define(App\City::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'province_id' => factory(\App\Province::class)->create()->id,
    ];
});

$factory->define(App\Provider::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'name' => $faker->word,
    ];
});
$factory->define(App\LoginHistory::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\User::class)->create()->id,
        'oauth_client_id' => factory(App\OauthClient::class)->create()->id,
    ];
});
$factory->define(App\ProductClass::class, function (Faker\Generator $faker) {
    return [
        'name' => 'App\NekaTopUpEntity'
        ];
});
$factory->define(App\ProductType::class, function (Faker\Generator $faker) {
    return [
        'title' => 'adsl'
    ];
});
$factory->define(App\PartaakProduct::class, function (Faker\Generator $faker) {
    return [
        'product_id' => 31,
        'title' => $faker->word(4),
        'price' => 10000,
        'provider_id' => factory(\App\Provider::class)->create()->id
    ];
});
$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'product_type_id' => factory(\App\ProductType::class)->create()->id,
        'provider_id' => factory(\App\Provider::class)->create()->id,
        'entity_id' => factory(\App\PartaakProduct::class)->create()->id,
        'entity_type' => factory(\App\ProductClass::class)->create(['name' => 'App\PartaakProduct'])->name,

    ];
});