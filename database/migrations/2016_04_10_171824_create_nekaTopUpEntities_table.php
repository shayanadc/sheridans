<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNekaTopUpEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('neka_top_up_entities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->integer('price');
            $table->boolean('visible')->default(true);
            $table->string('operator');
            $table->boolean('special')->default(false);
            $table->integer('provider_id')->unsigned();
            $table->timestamps();
            $table->unique(['title', 'operator', 'special']);

            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('neka_top_up_entities');
    }
}
