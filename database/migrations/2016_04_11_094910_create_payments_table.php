<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->nullable();
            $table->integer('amount');
            $table->integer('gw');
            $table->string('method');
            $table->string('tracking_code')->nullable();
            $table->string('receipt_number')->nullable();
            $table->string('pan')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->integer('registrant_id')->unsigned()->nullable();
            $table->integer('registrant_type')->index()->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
