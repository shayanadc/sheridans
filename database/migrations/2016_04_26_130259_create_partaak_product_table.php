<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartaakProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partaak_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('rate')->default(0);
            $table->string('rate_scale')->nullable();
            $table->integer('vol')->default(0);
            $table->string('scale')->nullable();
            $table->integer('category')->default(1);
            $table->integer('ussd_order')->default(0);
            $table->integer('period')->default(0);
            $table->string('title');
            $table->string('description')->nullable();
            $table->integer('price');
            $table->boolean('visible')->default(true);
            $table->integer('provider_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partaak_products');
    }
}
