<?php

use Illuminate\Database\Seeder;

class PartaakProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            ['product_id' => 550, 'rate' => 0, 'rate_scale' => 'کیلوبیت', 'vol' => 20, 'category' => 1, 'ussd_order' => 5, 'period' => 0, 'title' => '20 گیگ ترافیک عمومی پیش پرداخت', 'price' => 512300, 'visible' => 1],
            ['product_id' => 549, 'rate' => 0, 'rate_scale' => 'کیلوبیت', 'vol' => 10, 'category' => 1, 'ussd_order' => 4, 'period' => 0, 'title' => '10 گیگ ترافیک عمومی پیش پرداخت', 'price' => 337900, 'visible' => 1],
            ['product_id' => 544, 'rate' => 0, 'rate_scale' => 'کیلوبیت', 'vol' => 5, 'category' => 1, 'ussd_order' => 3, 'period' => 0, 'title' => '5 گیگ ترافیک عمومی پیش پرداخت', 'price' => 196200, 'visible' => 1],
            ['product_id' => 542, 'rate' => 0, 'rate_scale' => 'کیلوبیت', 'vol' => 3, 'category' => 1, 'ussd_order' => 2, 'period' => 0, 'title' => '3 گیگ ترافیک عمومی پیش پرداخت', 'price' => 117720, 'visible' => 1],
            ['product_id' => 540, 'rate' => 0, 'rate_scale' => 'کیلوبیت', 'vol' => 1, 'category' => 1, 'ussd_order' => 1, 'period' => 0, 'title' => '1 گیگ ترافیک عمومی پیش پرداخت', 'price' => 39240, 'visible' => 1],
            ['product_id' => 79, 'rate' => 64, 'rate_scale' => 'کیلوبیت', 'vol' => 9, 'category' => 2, 'ussd_order' => 0, 'period' => 3, 'title' => 'سرویس با سرعت 64 کیلوبیت و ترافیک 9 گیگابایت به مدت 3 ماه', 'price' => 277950, 'visible' => 1],
            ['product_id' => 92, 'rate' => 128, 'rate_scale' => 'کیلوبیت', 'vol' => 9, 'category' => 2, 'ussd_order' => 0, 'period' => 2, 'title' => 'سرویس با سرعت 64 کیلوبیت و ترافیک 9 گیگابایت به مدت 3 ماه', 'price' => 277950, 'visible' => 1],
            ['product_id' => 73, 'rate' => 256, 'rate_scale' => 'کیلوبیت', 'vol' => 9, 'category' => 2, 'ussd_order' => 0, 'period' => 1, 'title' => 'سرویس با سرعت 64 کیلوبیت و ترافیک 9 گیگابایت به مدت 3 ماه', 'price' => 277950, 'visible' => 1],
            ['product_id' => 75, 'rate' => 512, 'rate_scale' => 'کیلوبیت', 'vol' => 9, 'category' => 2, 'ussd_order' => 0, 'period' => 6, 'title' => 'سرویس با سرعت 64 کیلوبیت و ترافیک 9 گیگابایت به مدت 3 ماه', 'price' => 277950, 'visible' => 1],
        ];
        foreach ($array as $p) {
            DB::table('partaak_products')->insert($p);
        }
    }
}
