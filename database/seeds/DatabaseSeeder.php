<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(PartaakProductTableSeeder::class);
         $this->call(OauthClientsTableSeeder::class);
         $this->call(ProvinceTableSeeder::class);
         $this->call(CitiesTableSeeder::class);
         $this->call(ProductTypesTableSeeder::class);
         $this->call(ProductClassesTableSeeder::class);
         $this->call(PaymentGatewaysTableSeeder::class);
         $this->call(ProvidersTableSeeder::class);
    }
}
