<?php

use App\Provider;
use Illuminate\Database\Seeder;

class ProvidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $providers = [
            ['name' => 'مخابرات استان مازندران', 'title' => ''],
            ['name' => 'جهان نکا', 'title' => ''],
        ];

        foreach ($providers as $provider) {
            Provider::create($provider);
        }
    }
}
