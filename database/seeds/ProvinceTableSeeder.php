<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        $provinces = [
            ['name' => 'اردبیل', 'area_code' => 45, 'visible' => FALSE],
            ['name' => 'اصفهان', 'area_code' => 31, 'visible' => FALSE],
            ['name' => 'البرز', 'area_code' => 26, 'visible' => FALSE],
            ['name' => 'ایلام', 'area_code' => 84, 'visible' => FALSE],
            ['name' => 'آذربایجان شرقی', 'area_code' => 41, 'visible' => FALSE],
            ['name' => 'آذربایجان غربی', 'area_code' => 44, 'visible' => FALSE],
            ['name' => 'بوشهر', 'area_code' => 77, 'visible' => FALSE],
            ['name' => 'تهران', 'area_code' => 21, 'visible' => FALSE],
            ['name' => 'چهار محال و بختیاری', 'area_code' => 38, 'visible' => FALSE],
            ['name' => 'خراسان جنوبی', 'area_code' => 56, 'visible' => FALSE],
            ['name' => 'خراسان رضوی', 'area_code' => 51, 'visible' => FALSE],
            ['name' => 'خراسان شمالی', 'area_code' => 58, 'visible' => FALSE],
            ['name' => 'خوزستان', 'area_code' => 61, 'visible' => FALSE],
            ['name' => 'زنجان', 'area_code' => 24, 'visible' => FALSE],
            ['name' => 'سمنان', 'area_code' => 23, 'visible' => FALSE],
            ['name' => 'سیستان و بلوچستان', 'area_code' => 54, 'visible' => FALSE],
            ['name' => 'فارس', 'area_code' => 71, 'visible' => FALSE],
            ['name' => 'قزوین', 'area_code' => 28, 'visible' => FALSE],
            ['name' => 'قم', 'area_code' => 25, 'visible' => FALSE],
            ['name' => 'کردستان', 'area_code' => 87, 'visible' => FALSE],
            ['name' => 'کرمان', 'area_code' => 34, 'visible' => FALSE],
            ['name' => 'کرمانشاه', 'area_code' => 83, 'visible' => FALSE],
            ['name' => 'کهگیلویه و بویراحمد', 'area_code' => 74, 'visible' => FALSE],
            ['name' => 'گلستان', 'area_code' => 17, 'visible' => TRUE],
            ['name' => 'گیلان', 'area_code' => 13, 'visible' => FALSE],
            ['name' => 'لرستان', 'area_code' => 66, 'visible' => FALSE],
            ['name' => 'مازندران', 'area_code' => 11, 'visible' => TRUE],
            ['name' => 'مرکزی', 'area_code' => 86, 'visible' => FALSE],
            ['name' => 'هرمزگان', 'area_code' => 76, 'visible' => FALSE],
            ['name' => 'همدان', 'area_code' => 81, 'visible' => FALSE],
            ['name' => 'یزد', 'area_code' => 35, 'visible' => FALSE],
        ];

        foreach ($provinces as $province) {
            $province = array_merge($province, ['created_at' => $now, 'updated_at' => $now]);
            DB::table('provinces')->insert($province);
        }
    }
}
