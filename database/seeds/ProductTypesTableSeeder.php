<?php

use App\ProductType;
use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['title' => 'ADSL'],
            ['title' => 'Mobile Top-Up'],
        ];

        foreach ($types as $type) {
            ProductType::create($type);
        }
    }
}
