<?php

use App\ProductClass;
use Illuminate\Database\Seeder;

class ProductClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
            ['name' => 'App\PartaakProduct'],
            ['name' => 'App\NekaTopUpEntity'],
        ];

        foreach ($classes as $class) {
            ProductClass::create($class);
        }

    }
}
