<?php

use App\PaymentGateway;
use Illuminate\Database\Seeder;

class PaymentGatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gateways = [
            ['title' => 'بانک صادرات', 'mid' => 'AA5F', 'username' => null, 'password' => 'a0u0w9q6', 'fallback' => false, 'class_name' => 'App\IranKishPG', 'acno' => '', 'enabled' => true],
            ['title' => 'بانک صادرات', 'mid' => 'AA63', 'username' => null, 'password' => 'r1g5y3u1', 'fallback' => false, 'class_name' => 'App\IranKishPG', 'acno' => '', 'enabled' => false],
            ['title' => 'بانک سپه اینترنتی', 'mid' => 'AA60', 'username' => null, 'password' => 'i5z3w2m6', 'fallback' => false, 'class_name' => 'App\IranKishPG', 'acno' => '', 'enabled' => true],
            ['title' => 'بانک صادرات', 'mid' => 'AA7A', 'username' => null, 'password' => 'c1x0i6u0', 'fallback' => false, 'class_name' => 'App\IranKishPG', 'acno' => '', 'enabled' => true],
            ['title' => 'بانک شهر', 'mid' => '10415462', 'username' => null, 'password' => '5275208', 'fallback' => true, 'class_name' => 'App\SamanKishPG', 'acno' => '', 'enabled' => true],
            ['title' => 'پارسیان', 'mid' => '1111', 'username' => null, 'password' => '1111', 'fallback' => false, 'class_name' => 'App\UssdPG', 'acno' => '', 'enabled' => false],
            ['title' => 'صندوق Adsl', 'mid' => '2222', 'username' => null, 'password' => '2222', 'fallback' => false, 'class_name' => 'App\DepositPG', 'acno' => '', 'enabled' => false],
            ['title' => 'بانک سپه کارت', 'mid' => '3333', 'username' => null, 'password' => '3333', 'fallback' => false, 'class_name' => 'App\PosPG', 'acno' => '', 'enabled' => false],
        ];

        foreach ($gateways as $gateway) {
            PaymentGateway::create($gateway);
        }
    }
}
