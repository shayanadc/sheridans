<?php

use App\OauthClient;
use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = [
            ['id' => 'UmUlVE9TNtSrBvLL', 'secret' => 'Dh3bzPfjLdKgM1DRhKLuz2ZOcrDIzyot67cpcIeY', 'name' => 'POSTMAN'],
            ['id' => '0bvsml8oxW9WQuXr', 'secret' => 'Q0p1P14JoqYaEGOTNUkLAJLS3PpwD69XCbktJ6P8', 'name' => 'ANDROID APP'],
            ['id' => 'd4BwdvRFycMtqaVF', 'secret' => 'Bm5dTWhvSu4YJDJ4TsWdx3CAJE1cLYVSZQ9pdYl2', 'name' => 'WEB APP'],
        ];

        foreach ($clients as $client) {
            OauthClient::create($client);
        }
    }
}
