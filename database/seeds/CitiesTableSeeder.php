<?php

use App\City;
use App\Province;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $provinces = [
            [
                'name' => 'گلستان',
                'cities' => [
                    'گرگان',
                    'بندر ترکمن',
                    'علی آباد',
                    'فاضل آباد',
                    'کردکوی',
                    'بندر گز',
                    'آق قلا',
                ],
            ],
            [
                'name' => 'مازندران',
                'cities' => [
                    'ساری',
                ],
            ],
        ];

        foreach ($provinces as $province) {
            $p = Province::where('name', $province['name'])->firstOrFail();

            $cs = [];
            foreach ($province['cities'] as $city) {
                $cs[] = new City(['name' => $city]);
            }

            $p->cities()->saveMany($cs);
        }
    }
}
