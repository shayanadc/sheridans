<?php

namespace spec\App;

use App\User;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('App\User');
    }

    function it_has_a_nullable_mobile_number()
    {
        $u = factory(User::class)->create();

        $u->mobile->shouldBeNull();

        $u = factory(User::class)->create(['mobile' => '09121234567']);

        $u->mobile->shouldBe('09121234567');
    }

    function it_has_a_nullable_email_address()
    {
        $u = factory(User::class)->create();

        $u->email->shouldBeNull();

        $u = factory(User::class)->create(['email' => 'test@example.com']);

        $u->email->shouldBe('test@example.com');
    }

    function it_has_no_activation_code()
    {
        $this->activationCode->shouldBeNull();
    }

    function it_can_have_one_activation_code(ActivationCode $ac)
    {
        $this->activationCode()->save($ac);

        $this->activationCode->shouldBe($ac);
    }

    function it_can_have_only_one_activation_code(ActivationCode $ac1, ActivationCode $ac2)
    {
        $this->activationCode()->save($ac1);

        $this->activationCode()->save($ac2);

        $this->activationCode->shouldBe($ac2);
    }
}

